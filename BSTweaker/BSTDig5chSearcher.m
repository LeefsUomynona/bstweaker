//
//  BSTDig5chSearcher.m
//  BSTweaker
//
//  Created by anonymous on 2018/03/08.
//  Copyright © 2018年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BSTPlugIns.h"

@interface BSTDig5chSearcher ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@end

@implementation BSTDig5chSearcher

+ (id)sharedInstance
{
    static BSTDig5chSearcher* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTDig5chSearcher alloc] init];
    });
    return sharedInstance;
}

- (void)swizzle
{
    if(_status != BSTPluginStatusDisabled) return;
    
    Class SyoboiSoulGem = objc_lookUpClass("BSSyoboiSoulGem");
    Class TGrepResult = objc_lookUpClass("BSTGrepResult");
    Class TGrepClientWindowController = objc_lookUpClass("BSTGrepClientWindowController");
    if(!SyoboiSoulGem || !TGrepResult || !TGrepClientWindowController) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    
    NSString *(^queryStringForSearchString_mod)(id, NSString *) = ^(id self, NSString *searchStr) {
        NSString *encodedString = ((NSString* (*)(id, SEL, NSStringEncoding))objc_msgSend)(searchStr, @selector(stringByURLEncodingUsingEncoding:), NSUTF8StringEncoding);
        if (!encodedString || [encodedString isEqualToString:@""]) {
            return (NSString *)nil;
        }
        int sort = 4;
        switch ((int)[self performSelector:@selector(searchOptionType)]) {
            case 0:
                sort = 4;
                break;
            case 1:
                sort = 1;
                break;
            case 2:
                sort = 5;
                break;
            case 3:
                sort = 2;
                break;
            default:
                break;
        }
        NSString *queryString = [NSString stringWithFormat:@"https://dig.5ch.net/?keywords=%@&maxResult=100&AndOr=0&atLeast=1&Sort=%d&Link=1&Bbs=all&924=0", encodedString, sort];
        return queryString;
    };
    SWIZZLE_METHOD(SyoboiSoulGem, queryStringForSearchString:, queryStringForSearchString_mod, "@@:@");
    
    BOOL (^canHandleSearchOptionType_mod)(id, NSUInteger) = ^(id self, NSUInteger type) {
        return YES;
    };
    SWIZZLE_METHOD(SyoboiSoulGem, canHandleSearchOptionType:, canHandleSearchOptionType_mod, "c@:Q");
    
    NSArray *(^parseHTMLSource_mod)(id, NSString *, NSError **) = ^(id self, NSString *source, NSError **errorPtr) {
        NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithXMLString:source options:NSXMLDocumentTidyHTML error:errorPtr];
        if (!xmlDoc) {
            return [NSArray array];
        }
        NSArray *results = [xmlDoc objectsForXQuery:@".//li[@class='digRes1']" error:errorPtr];
        if (!results) {
            return [NSArray array];
        }
        NSMutableArray *resultArr = [NSMutableArray arrayWithCapacity:[results count]];
        [results enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSArray *link1 = [obj objectsForXQuery:@".//a[@href]" error:errorPtr];
            NSArray *link2 = [obj objectsForXQuery:@".//span[@class='motoT']/a[@href]" error:errorPtr];
            NSString *threadTitle = [[link1 objectAtIndex:0] stringValue];
            NSString *threadURL = [[[link2 objectAtIndex:0] attributeForName:@"href"] stringValue];
            id result = ((id (*)(id, SEL))objc_msgSend)(TGrepResult, @selector(alloc));
            result = ((id (*)(id, SEL, NSUInteger, id, id))objc_msgSend)(result, @selector(initWithOrderNo:URL:titleWithoutBoldTag:), idx+1, threadURL, threadTitle);
            if([result respondsToSelector:@selector(setRate:)]) {
                long idx = threadTitle.length-1;
                if([threadTitle characterAtIndex:idx] == ')') {
                    for(;idx>=0;idx--) {
                        unichar c = [threadTitle characterAtIndex:idx];
                        if(c == '(') break;
                    }
                    idx++;
                    int resCount = [[threadTitle substringFromIndex:idx] intValue];
                    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"/test/read\\.cgi/[\\w]+/([0-9]+)/" options:0 error:nil];
                    NSTextCheckingResult *match = [regex firstMatchInString:threadURL options:0 range:NSMakeRange(0, threadURL.length)];
                    if(match) {
                        NSInteger life = time(0) - [[threadURL substringWithRange:[match rangeAtIndex:1]] integerValue];
                        double rate = resCount*3600.0/life;
                        ((void (*)(id, SEL, double))objc_msgSend)(result, @selector(setRate:), rate);
                    }
                    
                }
            }
            [resultArr addObject:result];
        }];
        return (NSArray *)resultArr;
    };
    SWIZZLE_METHOD(SyoboiSoulGem, parseHTMLSource:error:, parseHTMLSource_mod, "@@:@:^id");
    
    void (^windowDidLoad_mod)(id) = ^(id self) {
        [self performSelector:@selector(mod_windowDidLoad)];
        NSToolbar *toolbar = [[self performSelector:@selector(window)] toolbar];
        toolbar.allowsUserCustomization = YES;
        id allowedItems = [toolbar performSelector:@selector(_allowedItems)];
        for(NSToolbarItem *item in allowedItems) {
            if([item.view isKindOfClass:[NSPopUpButton class]]) {
                if(![toolbar.items containsObject:item]) {
                    [toolbar insertItemWithItemIdentifier:item.itemIdentifier atIndex:1];
                }
            }
        }
        
        NSArray *views = [[[self performSelector:@selector(progressIndicator)] superview] subviews];
        for(id obj in views) {
            if([obj isKindOfClass:[NSButton class]]) {
                [obj setTitle:@"サービスの提供元: dig.5ch.net"];
            }
        }
    };
    SWIZZLE_METHOD(TGrepClientWindowController, windowDidLoad, windowDidLoad_mod, "@@");
    
    void (^openSearchProvider_mod)(id, id) = ^(id self, id sender) {
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"https://dig.5ch.net/"]];
    };
    SWIZZLE_METHOD(TGrepClientWindowController, openSearchProvider:, openSearchProvider_mod, "@@:@");
    
    self.status = BSTPluginStatusEnabled;
}

@end
