//
//  BSTThrobberRemover.m
//  BSTweaker
//
//  Created by anonymous on 2018/02/10.
//  Copyright © 2018年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BSTPlugIns.h"

@interface BSTThrobberRemover ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@end

@implementation BSTThrobberRemover

+ (id)sharedInstance
{
    static BSTThrobberRemover* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTThrobberRemover alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if(self) {
        float bundleVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] floatValue];
        float shortVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] floatValue];
        if(bundleVersion < 1057 || shortVersion < 3.0f || bundleVersion > 1057) {
            _status = BSTPluginStatusBlockedByVersion;
        }
    }
    return self;
}

- (void)swizzle
{
    if(_status != BSTPluginStatusDisabled) return;
    
    Class StatusLineWindowController = objc_lookUpClass("CMRStatusLineWindowController");
    if(!StatusLineWindowController) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    
    NSProgressIndicator* (^progressIndicator_mod)(id) = ^(id self) {
        NSProgressIndicator *i = [self performSelector:@selector(mod_progressIndicator)];
        NSView *superview = i.superview.superview;
        if([superview isKindOfClass:[NSBox class]]) superview.hidden = YES;
        return i;
    };
    SWIZZLE_METHOD(StatusLineWindowController, progressIndicator, progressIndicator_mod, "@@");
    
    self.status = BSTPluginStatusEnabled;
}

@end
