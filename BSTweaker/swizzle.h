//
//  swizzle.h
//  BSTweaker
//
//  Created by anonymous on 2017/12/04.
//  Copyright © 2017年 anonymous. All rights reserved.
//

#import <objc/runtime.h>
#import <objc/message.h>

#pragma clang diagnostic ignored "-Wundeclared-selector"

#define SWIZZLE_METHOD(class, SEL, block, type) {\
    IMP imp = imp_implementationWithBlock(block);\
    class_addMethod(class, @selector( mod_ ## SEL ), imp, type);\
    Method fromMethod = class_getInstanceMethod(class, @selector( SEL ));\
    Method toMethod   = class_getInstanceMethod(class, @selector( mod_ ## SEL ));\
    if(fromMethod && toMethod) method_exchangeImplementations(fromMethod, toMethod);\
    else { self.status = BSTPluginStatusFailedToLoad; return; }\
}

#define SWIZZLE_METHOD2(class, SEL, SEL_mod, block, type) {\
    IMP imp = imp_implementationWithBlock(block);\
    class_addMethod(class, @selector( SEL_mod ), imp, type);\
    Method fromMethod = class_getInstanceMethod(class, @selector( SEL ));\
    Method toMethod   = class_getInstanceMethod(class, @selector( SEL_mod ));\
    if(fromMethod && toMethod) method_exchangeImplementations(fromMethod, toMethod);\
    else { self.status = BSTPluginStatusFailedToLoad; return; }\
}

typedef enum {
    BSTPluginStatusDisabled = 0,
    BSTPluginStatusEnabled,
    BSTPluginStatusBlockedByVersion,
    BSTPluginStatusFailedToLoad,
} BSTPluginStatus;

@protocol BSTMethodSwizzling <NSObject>
@property(nonatomic, readonly) BSTPluginStatus status;
+ (id <BSTMethodSwizzling>)sharedInstance;
- (void)swizzle;
@end
