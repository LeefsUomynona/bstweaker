//
//  BSTAdvancedNGFilter.m
//  BSTweaker
//
//  Created by anonymous on 2017/12/04.
//  Copyright © 2017年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BSTPlugIns.h"

@interface BSTAdvancedNGFilter ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@end

@implementation BSTAdvancedNGFilter

+ (id)sharedInstance
{
    static BSTAdvancedNGFilter* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTAdvancedNGFilter alloc] init];
    });
    return sharedInstance;
}

- (void)swizzle
{
    if(_status != BSTPluginStatusDisabled) return;
    
    Class SpamJudge = objc_lookUpClass("BSSpamJudge");
    if(!SpamJudge) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    
    id (^initWithThreadSignature_mod)(id, id) = ^(id self, id signature) {
        id obj = [self performSelector:@selector(initWithThreadSignature_mod:) withObject:signature];
        if(obj) {
            objc_setAssociatedObject(obj, "THREAD_SIGNATURE", signature, OBJC_ASSOCIATION_RETAIN);
        }
        return obj;
    };
    SWIZZLE_METHOD2(SpamJudge, initWithThreadSignature:, initWithThreadSignature_mod:, initWithThreadSignature_mod, "@@:@");
    
    BOOL (^isSpamWithNGExpressionsMatch_mod)(id, id) = ^(id self, id message) {
        BOOL ret = (BOOL)[self performSelector:@selector(mod_isSpamWithNGExpressionsMatch:) withObject:message];
        if(ret) return YES;
        BOOL hasBasicInfoForAdvancedFilter = NO;
        id signature;
        NSString *identifier = nil;
        NSString *sourceBody = nil;
        NSString *sourceName = nil;
        NSString *convertedName = nil;
        NSString *convertedMail = nil;
        NSString *sourceID = [message performSelector:@selector(IDString)];
        if(!sourceID) sourceID = @"";
        
        unsigned int flag = 0;
        NSArray *expressions = nil;
        if([self respondsToSelector:@selector(NGExpressions)]) {
            expressions = [self performSelector:@selector(NGExpressions)];
        } else {
            id signature = objc_getAssociatedObject(self, "THREAD_SIGNATURE");
            if(signature) expressions = [self performSelector:@selector(mergedNGExpressionsForThreadSignature:) withObject:signature];
        }
        for(id expression in expressions) {
            BOOL shouldCheckName = (BOOL)[expression performSelector:@selector(checksName)];
            BOOL shouldCheckMail = (BOOL)[expression performSelector:@selector(checksMail)];
            BOOL shouldCheckMessage = (BOOL)[expression performSelector:@selector(checksMessage)];
            if(shouldCheckName || shouldCheckMail || shouldCheckMessage) continue;
            NSString *NGExpression = [expression performSelector:@selector(ngExpression)];
            if ([NGExpression rangeOfString:@"!ext:" options:NSLiteralSearch].location != 0) {
                id regex = [expression performSelector:@selector(regex)];
                if(regex) {
                    if([regex respondsToSelector:@selector(search:)]) {
                        if([regex performSelector:@selector(search:) withObject:sourceID]) {
                            return YES;
                        }
                    }
                    else if([regex respondsToSelector:@selector(numberOfMatchesInString:options:range:)]) {
                        if(((NSUInteger (*)(id, SEL, id, NSMatchingOptions, NSRange))objc_msgSend)(regex, @selector(numberOfMatchesInString:options:range:), sourceID, 0, NSMakeRange(0, [sourceID length]))) {
                            return YES;
                        }
                    }
                }
                else {
                    if(!NGExpression) continue;
                    if ([sourceID rangeOfString:NGExpression options:NSLiteralSearch].location != NSNotFound) {
                        return YES;
                    }
                }
            }
            else {
                BOOL match = NO;
                BOOL shouldCheckID = NO;
                unsigned int option = 0;
                unsigned int mask = 0;
                const char *str = [NGExpression UTF8String];
                const char *ptr = str+strlen("!ext:");
                const char *start = ptr;
                
                if(!hasBasicInfoForAdvancedFilter) {
                    sourceBody = [message performSelector:@selector(cachedMessage)];
                    sourceName = [message performSelector:@selector(name)];
                    NSString *sourceMail = [message performSelector:@selector(mail)];
                    
                    Class CMXTextParser = objc_lookUpClass("CMXTextParser");
                    if (sourceName && [sourceName length] > 0) {
                        convertedName = [CMXTextParser performSelector:@selector(cachedMessageWithMessageSource:) withObject:sourceName];
                    } else {
                        convertedName = nil;
                    }
                    if (sourceMail) {
                        NSUInteger mailLength = [sourceMail length];
                        if (mailLength > 4) {
                            convertedMail = [CMXTextParser performSelector:@selector(stringByReplacingEntityReference:) withObject:sourceMail];
                        } else {
                            convertedMail = sourceMail;
                        }
                    } else {
                        convertedMail = nil;
                    }
                    signature = objc_getAssociatedObject(self, "THREAD_SIGNATURE");
                    if(signature) identifier = [signature performSelector:@selector(identifier)];
                    hasBasicInfoForAdvancedFilter = YES;
                }
                
                while(*ptr != ':' && *ptr != 0) ptr++;
                if(*ptr == 0) continue;
                if((ptr-start) == 1 && *start == '*') {
                    // do nothing
                }
                else if(identifier) {
                    BOOL isTarget = NO;
                    NSString *threadKeys = [[NSString alloc] initWithBytes:start length:ptr-start encoding:NSUTF8StringEncoding];
                    NSArray *keysArr = [threadKeys componentsSeparatedByString:@","];
                    for(NSString *key in keysArr) {
                        if([key isEqualToString:identifier]) {
                            isTarget = YES;
                            break;
                        }
                    }
                    if(!isTarget) continue;
                }
                ptr++;
                start = ptr;
                while(*ptr != ':' && *ptr != 0) ptr++;
                if(*ptr == 0) continue;
                if(ptr-start == 0) continue;
                if((ptr-start) == 1 && *start == '*') {
                    shouldCheckName = YES;
                    shouldCheckMail = YES;
                    shouldCheckMessage = YES;
                    shouldCheckID = YES;
                }
                else {
                    NSString *target = [[[NSString alloc] initWithBytes:start length:ptr-start encoding:NSUTF8StringEncoding] lowercaseString];
                    NSArray *targetArr = [target componentsSeparatedByString:@","];
                    for(NSString *s in targetArr) {
                        if([s isEqualToString:@"name"]) shouldCheckName = YES;
                        else if([s isEqualToString:@"mail"]) shouldCheckMail = YES;
                        else if([s isEqualToString:@"message"]) shouldCheckMessage = YES;
                        else if([s isEqualToString:@"id"]) shouldCheckID = YES;
                    }
                }
                ptr++;
                start = ptr;
                while(*ptr != ':' && *ptr != 0) ptr++;
                if(*ptr == 0) continue;
                if(ptr-start == 0) continue;
                option = (unsigned int)strtoul(start, NULL, 16);
                mask = option & 0xffff0000;
                NGExpression = [NSString stringWithUTF8String:ptr+1];
                if(!NGExpression) continue;
                
                if (option & 0x1) {
                    NSRegularExpressionOptions regexOptions = 0;
                    if(option & 0x2) regexOptions |= NSRegularExpressionCaseInsensitive;
                    NSRegularExpression *regex = objc_getAssociatedObject(expression, "ADVANCED_NGFILTER_REGEX");
                    if(!regex || ![regex.pattern isEqualToString:NGExpression] || regex.options != regexOptions) {
                        regex = [NSRegularExpression regularExpressionWithPattern:NGExpression options:regexOptions error:nil];
                        if(regex) {
                            objc_setAssociatedObject(expression, "ADVANCED_NGFILTER_REGEX", regex, OBJC_ASSOCIATION_RETAIN);
                        }
                        else continue;
                    }
                    
                    if (sourceBody && shouldCheckMessage) {
                        if ([regex rangeOfFirstMatchInString:sourceBody options:0 range:NSMakeRange(0, [sourceBody length])].location != NSNotFound) {
                            match = YES;
                            goto last;
                        }
                    }
                    if (shouldCheckName) {
                        NSString *target = (option & 0x4) ? sourceName : convertedName;
                        if (target && [regex rangeOfFirstMatchInString:target options:0 range:NSMakeRange(0, [target length])].location != NSNotFound) {
                            match = YES;
                            goto last;
                        }
                    }
                    if (convertedMail && shouldCheckMail) {
                        if ([regex rangeOfFirstMatchInString:convertedMail options:0 range:NSMakeRange(0, [convertedMail length])].location != NSNotFound) {
                            match = YES;
                            goto last;
                        }
                    }
                    if (sourceID && shouldCheckID) {
                        if ([regex rangeOfFirstMatchInString:sourceID options:0 range:NSMakeRange(0, [sourceID length])].location != NSNotFound) {
                            match = YES;
                            goto last;
                        }
                    }
                } else {
                    NSStringCompareOptions searchOptions = NSLiteralSearch;
                    if(option & 0x2) searchOptions |= NSCaseInsensitiveSearch;
                    if (sourceBody && shouldCheckMessage) {
                        if ([sourceBody rangeOfString:NGExpression options:searchOptions].location != NSNotFound) {
                            match = YES;
                            goto last;
                        }
                    }
                    if (shouldCheckName) {
                        NSString *target = (option & 0x4) ? sourceName : convertedName;
                        if (target && [target rangeOfString:NGExpression options:searchOptions].location != NSNotFound) {
                            match = YES;
                            goto last;
                        }
                    }
                    if (convertedMail && shouldCheckMail) {
                        if ([convertedMail rangeOfString:NGExpression options:searchOptions].location != NSNotFound) {
                            match = YES;
                            goto last;
                        }
                    }
                    if (sourceID && shouldCheckID) {
                        if ([sourceID rangeOfString:NGExpression options:searchOptions].location != NSNotFound) {
                            match = YES;
                            goto last;
                        }
                    }
                }
            last:
                if(match) {
                    if(mask) {
                        flag |= mask;
                        if     ((flag & 0x000f0000) == 0x000f0000) ret = YES;
                        else if((flag & 0x00f00000) == 0x00f00000) ret = YES;
                        else if((flag & 0x0f000000) == 0x0f000000) ret = YES;
                        else if((flag & 0xf0000000) == 0xf0000000) ret = YES;
                    }
                    else ret = YES;
                    if(ret && (option & 0x8)) {
                        ((void (*)(id, SEL, BOOL))objc_msgSend)(message, @selector(setInvisibleAboned:), YES);
                    }
                    if(ret) return YES;
                }
            }
        }
        return ret;
    };
    SWIZZLE_METHOD(SpamJudge, isSpamWithNGExpressionsMatch:, isSpamWithNGExpressionsMatch_mod, "c@:@");
    
    self.status = BSTPluginStatusEnabled;
}

@end
