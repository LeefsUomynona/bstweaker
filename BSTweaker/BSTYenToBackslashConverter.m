//
//  BSTYenToBackslashConverter.m
//  BSTweaker
//
//  Created by anonymous on 2018/12/25.
//  Copyright © 2018年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BSTPlugIns.h"

@interface BSTYenToBackslashConverter ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@end

@implementation BSTYenToBackslashConverter

+ (id)sharedInstance
{
    static BSTYenToBackslashConverter* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTYenToBackslashConverter alloc] init];
    });
    return sharedInstance;
}

- (void)swizzle
{
    Class ThreadView = objc_lookUpClass("CMRThreadView");
    if(!ThreadView) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    
    BOOL (^writeSelectionToPasteboard_mod)(id, NSPasteboard *, NSString *) = ^(id self, NSPasteboard *pboard, NSString *type) {
        BOOL ret = ((BOOL (*)(id, SEL, NSPasteboard *, NSString *))objc_msgSend)(self, @selector(mod_writeSelectionToPasteboard:type:), pboard, type);
        if([type isEqualToString:NSStringPboardType]) {
            NSString *replaced = [[pboard stringForType:type] stringByReplacingOccurrencesOfString:@"\u00a5" withString:@"\\"];
            return [pboard setString:replaced forType:type];
        }
        return ret;
    };
    SWIZZLE_METHOD(ThreadView, writeSelectionToPasteboard:type:, writeSelectionToPasteboard_mod, "c@:@:@");
    
    self.status = BSTPluginStatusEnabled;
}

@end
