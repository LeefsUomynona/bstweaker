//
//  BSTAlertSuppressor.m
//  BSTweaker
//
//  Created by anonymous on 2018/03/20.
//  Copyright © 2018年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BSTPlugIns.h"

@interface BSTAlertSuppressor ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@end

@implementation BSTAlertSuppressor

+ (id)sharedInstance
{
    static BSTAlertSuppressor* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTAlertSuppressor alloc] init];
    });
    return sharedInstance;
}

- (void)swizzle
{
    Class ThreadViewer = objc_lookUpClass("CMRThreadViewer");
    if(!ThreadViewer) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    
    void (^threadTextDownloaderHighIntensity_mod)(id, NSNotification *) = ^(id self, NSNotification *notification) {
        if([notification.object isKindOfClass:objc_lookUpClass("BSAPIDownloader")] && (
           [notification.name isEqualToString:@"jp.tsawada2.BathyScaphe.CMRDATDownloaderDidDetectInvalidAPIServerStatusNotification"] ||
           [notification.name isEqualToString:@"jp.tsawada2.BathyScaphe.CMRDATDownloaderDidDetect5xxErrorNotification"])) {
            NSError *error = notification.userInfo[@"Error"];
            objc_setAssociatedObject(error, "API_SERVER_ERROR_DELEGATE", self, OBJC_ASSOCIATION_ASSIGN);
        }
        [self performSelector:@selector(mod_threadTextDownloaderHighIntensity:) withObject:notification];
    };
    SWIZZLE_METHOD(ThreadViewer, threadTextDownloaderHighIntensity:, threadTextDownloaderHighIntensity_mod, "@@:@");
    
    void (^threadTextDownloaderConnectionDidFail_mod)(id, NSNotification *) = ^(id self, NSNotification *notification) {
        if([notification.object isKindOfClass:objc_lookUpClass("BSAPIDownloader")] &&
           [notification.name isEqualToString:@"CMRDownloaderConnectionDidFailNotification"]) {
            NSError *error = notification.userInfo[@"Error"];
            objc_setAssociatedObject(error, "API_SERVER_ERROR_DELEGATE", self, OBJC_ASSOCIATION_ASSIGN);
        }
        [self performSelector:@selector(mod_threadTextDownloaderConnectionDidFail:) withObject:notification];
    };
    SWIZZLE_METHOD(ThreadViewer, threadTextDownloaderConnectionDidFail:, threadTextDownloaderConnectionDidFail_mod, "@@:@");
    
    id (^alertWithError_mod)(id, NSError*) = ^(id self, NSError *error) {
        id obj = [self performSelector:@selector(mod_alertWithError:) withObject:error];
        id viewer = objc_getAssociatedObject(error, "API_SERVER_ERROR_DELEGATE");
        if(viewer) {
            objc_setAssociatedObject(obj, "API_SERVER_ERROR_DELEGATE", viewer, OBJC_ASSOCIATION_ASSIGN);
        }
        return obj;
    };
    SWIZZLE_METHOD(object_getClass([NSAlert class]), alertWithError:, alertWithError_mod, "@@:@");
    
    if([ThreadViewer instancesRespondToSelector:@selector(highIntensityAlertDidEnd:returnCode:contextInfo:)]) {
        void (^beginSheetModalForWindow_mod)(NSAlert*, NSWindow*, id, SEL, void*) = ^(NSAlert *self, NSWindow* sheetWindow, id delegate, SEL selector, void *contextInfo) {
            id viewer = objc_getAssociatedObject(self, "API_SERVER_ERROR_DELEGATE");
            if(viewer) {
                NSString *message = [NSString stringWithFormat:@"%@ %@",self.messageText,self.informativeText];
                id ruler = [[viewer scrollView] horizontalRulerView];
                NSUInteger mode = ((NSUInteger (*)(id, SEL))objc_msgSend)([viewer class], @selector(rulerModeForInformDatOchi));
                ((void (*)(id, SEL, NSUInteger))objc_msgSend)(ruler, @selector(setCurrentMode:), mode);
                ((void (*)(id, SEL, NSString*))objc_msgSend)(ruler, @selector(setInfoStr:), message);
                ((void (*)(id, SEL, BOOL))objc_msgSend)([viewer scrollView], @selector(setRulersVisible:), YES);
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5.0f * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    ((void (*)(id, SEL, NSString*))objc_msgSend)(viewer, @selector(cleanUpTitleRuler:), nil);
                });
            } else {
                ((void (*)(id, SEL, NSWindow*, id, SEL, void*))objc_msgSend)(self, @selector(mod_beginSheetModalForWindow:modalDelegate:didEndSelector:contextInfo:), sheetWindow, delegate, selector, contextInfo);
            }
        };
        SWIZZLE_METHOD([NSAlert class], beginSheetModalForWindow:modalDelegate:didEndSelector:contextInfo:, beginSheetModalForWindow_mod, "@@:@:@:@:^void");
    } else {
        void (^beginSheetModalForWindow_mod)(NSAlert*, NSWindow*, void (^)(NSModalResponse)) = ^(NSAlert *self, NSWindow* sheetWindow, void (^handler)(NSModalResponse)) {
            id viewer = objc_getAssociatedObject(self, "API_SERVER_ERROR_DELEGATE");
            if(viewer) {
                NSString *message = [NSString stringWithFormat:@"%@ %@",self.messageText,self.informativeText];
                id ruler = [[viewer scrollView] horizontalRulerView];
                NSUInteger mode = ((NSUInteger (*)(id, SEL))objc_msgSend)([viewer class], @selector(rulerModeForInformDatOchi));
                ((void (*)(id, SEL, NSUInteger))objc_msgSend)(ruler, @selector(setCurrentMode:), mode);
                ((void (*)(id, SEL, NSString*))objc_msgSend)(ruler, @selector(setInfoStr:), message);
                ((void (*)(id, SEL, BOOL))objc_msgSend)([viewer scrollView], @selector(setRulersVisible:), YES);
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5.0f * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    ((void (*)(id, SEL, NSString*))objc_msgSend)(viewer, @selector(cleanUpTitleRuler:), nil);
                });
            } else {
                ((void (*)(id, SEL, NSWindow*, void (^)(NSModalResponse)))objc_msgSend)(self, @selector(mod_beginSheetModalForWindow:completionHandler:), sheetWindow, handler);
            }
        };
        SWIZZLE_METHOD([NSAlert class], beginSheetModalForWindow:completionHandler:, beginSheetModalForWindow_mod, "@@:@:?");
    }
    
    self.status = BSTPluginStatusEnabled;
}

@end
