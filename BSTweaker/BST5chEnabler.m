//
//  BST5chEnabler.m
//  BSTweaker
//
//  Created by anonymous on 2017/12/02.
//  Copyright © 2017年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BSTPlugIns.h"

@interface BST5chEnabler ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@property(nonatomic, readonly) float bundleVersion;
@property(nonatomic, copy) NSString *headerModificationPlistPath;
@property(nonatomic, strong) NSMutableDictionary *headerModificationRules;
@property(nonatomic, strong) NSDate *headerModificationRuleModificationDate;
@end

@implementation BST5chEnabler

+ (id)sharedInstance
{
    static BST5chEnabler* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BST5chEnabler alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if(self) {
        _bundleVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] floatValue];
        /*if(_bundleVersion > 1057) {
            _status = BSTPluginStatusBlockedByVersion;
        }*/
        _headerModificationPlistPath = [[[[[NSBundle bundleForClass:[BSTThreadListAppearanceChanger class]] bundlePath] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"BSTweaker-RequestHeaderModification.plist"];
        _headerModificationRules = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (NSInteger)reloadHeaderModificationRules
{
    NSMutableDictionary *plistDic = [[NSMutableDictionary alloc] initWithContentsOfFile:_headerModificationPlistPath];
    if(!plistDic) {
        [_headerModificationRules removeAllObjects];
        return 0;
    }
    NSDictionary *plistAttr = [[NSFileManager defaultManager] attributesOfItemAtPath:_headerModificationPlistPath error:nil];
    NSDate *modificationDate = [plistAttr objectForKey:NSFileModificationDate];
    if(_headerModificationRuleModificationDate && [_headerModificationRuleModificationDate compare:modificationDate] != NSOrderedAscending) {
        return -1;
    }
    self.headerModificationRuleModificationDate = modificationDate;
    [_headerModificationRules removeAllObjects];
    [_headerModificationRules addEntriesFromDictionary:plistDic];
    return _headerModificationRules.count;
}

- (void)swizzle
{
    if(_status != BSTPluginStatusDisabled) return;
    
    Class AppDefaults = objc_lookUpClass("AppDefaults");
    /* Force load 2ch connector plugin */
    id appDefaults = [AppDefaults performSelector:@selector(sharedInstance)];
    ((id (*)(id, SEL, NSURL*, NSDictionary*))objc_msgSend)(appDefaults, @selector(w2chConnectWithURL:properties:), [NSURL URLWithString:@"http://egg.2ch.net/test/bbs.cgi"], nil);
    
    Class URLManager = objc_lookUpClass("CMROpenURLManager");
    Class ThreadViewer = objc_lookUpClass("CMRThreadViewer");
    Class APIDownloader = objc_lookUpClass("BSAPIDownloader");
    Class BoardManager = objc_lookUpClass("BoardManager");
    Class TGrepResult = objc_lookUpClass("BSTGrepResult");
    Class Cookie = objc_lookUpClass("Cookie");
    Class ConcreteBoardListItem = objc_lookUpClass("ConcreteBoardListItem");
    Class SG2chErrorHandler = objc_lookUpClass("SG2chErrorHandler");
    Class SettingTxtDetector = objc_lookUpClass("BSSettingTxtDetector");
    Class LocalRulesCollector = objc_lookUpClass("BSLocalRulesCollector");
    Class DownloadTask = objc_lookUpClass("BSDownloadTask");
    Class SG2chConnector = objc_lookUpClass("SG2chConnector");
    Class ReplyMessenger = objc_lookUpClass("CMRReplyMessenger");
    Class CookieManager = objc_lookUpClass("CookieManager");
    Class JIMSidManager = objc_lookUpClass("JIMSidManager");
    if(!APIDownloader || !CookieManager || !JIMSidManager) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    if(_bundleVersion < 1065 && (!BoardManager || !Cookie || !SettingTxtDetector || !LocalRulesCollector || !DownloadTask || !SG2chConnector || !ReplyMessenger)) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    if(_bundleVersion < 1057 && (!URLManager || !ThreadViewer || !TGrepResult || !ConcreteBoardListItem || !SG2chErrorHandler)) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    
    if(_bundleVersion < 1057) {
        BOOL (^openLocation_mod)(id, NSURL *) = ^(id self, NSURL* url) {
            NSString *host = [url host];
            if([host hasSuffix:@".5ch.net"]) {
                NSString *newHost = [host stringByReplacingOccurrencesOfString:@".5ch.net" withString:@".2ch.net"];
                url = [[NSURL alloc] initWithScheme:[url scheme] host:newHost path:[url path]];
            }
            return (BOOL)[self performSelector:@selector(mod_openLocation:) withObject:url];
        };
        SWIZZLE_METHOD(URLManager, openLocation:, openLocation_mod, "c@:@");
        
        NSAttributedString* (^attributedStringWithLinkContext_mod)(id, id) = ^(id self, id link) {
            if([link isKindOfClass:[NSURL class]]) {
                NSString *host = [link host];
                if([host hasSuffix:@".5ch.net"]) {
                    NSString *newHost = [host stringByReplacingOccurrencesOfString:@".5ch.net" withString:@".2ch.net"];
                    link = [[NSURL alloc] initWithScheme:[link scheme] host:newHost path:[link path]];
                }
            }
            return ((id (*)(id, SEL, id))objc_msgSend)(self, @selector(mod_attributedStringWithLinkContext:), link);
        };
        BOOL (^textView_clickedOnLink_mod)(id, NSTextView *, id, NSUInteger) = ^(id self, NSTextView * textView, id link, NSUInteger index) {
            if([link isKindOfClass:[NSURL class]]) {
                NSString *host = [link host];
                if([host hasSuffix:@".5ch.net"]) {
                    NSString *newHost = [host stringByReplacingOccurrencesOfString:@".5ch.net" withString:@".2ch.net"];
                    link = [[NSURL alloc] initWithScheme:[link scheme] host:newHost path:[link path]];
                }
            }
            return ((BOOL (*)(id, SEL, id, id, NSUInteger))objc_msgSend)(self, @selector(mod_textView:clickedOnLink:atIndex:), textView, link, index);
        };
        SWIZZLE_METHOD(ThreadViewer, attributedStringWithLinkContext:, attributedStringWithLinkContext_mod, "@@:@");
        SWIZZLE_METHOD(ThreadViewer, textView:clickedOnLink:atIndex:, textView_clickedOnLink_mod, "c@:@:@:Q");
        
        id (^initWithURL_mod)(id, NSURL*, NSUInteger, double) = ^(id self, NSURL *URL, NSUInteger policy, double timeout) {
            if([URL.host isEqualToString:@"api.2ch.net"]) {
                NSString *path = URL.path;
                if([path isEqualToString:@"/v1/auth"]) {
                    URL = [NSURL URLWithString:@"https://api.5ch.net/v1/auth/"];
                }
                else URL = [[NSURL alloc] initWithScheme:URL.scheme host:@"api.5ch.net" path:path];
            }
            id obj = ((id (*)(id, SEL, id, NSUInteger, double))objc_msgSend)(self, @selector(initWithURL_mod:cachePolicy:timeoutInterval:), URL, policy, timeout);;
            return obj;
        };
        SWIZZLE_METHOD2([NSURLRequest class], initWithURL:cachePolicy:timeoutInterval:, initWithURL_mod:cachePolicy:timeoutInterval:, initWithURL_mod, "@@:@:Q:d");
        
        NSString* (^threadURLString_mod)(id) = ^(id self) {
            NSString *threadURL = [self performSelector:@selector(mod_threadURLString)];
            threadURL = [threadURL stringByReplacingOccurrencesOfString:@".5ch.net/" withString:@".2ch.net/"];
            return threadURL;
        };
        SWIZZLE_METHOD(TGrepResult, threadURLString, threadURLString_mod, "@@");
        
        id (^boardBoardListItemFromPlist_mod)(id, id) = ^(id self, id plist) {
            NSString *urlStr = [plist objectForKey:@"URL"];
            if(urlStr) {
                NSRange range = [urlStr rangeOfString:@".5ch.net/"];
                if(range.location != NSNotFound) {
                    plist = [plist mutableCopy];
                    [plist setObject:[urlStr stringByReplacingOccurrencesOfString:@".5ch.net/" withString:@".2ch.net/"] forKey:@"URL"];
                }
            }
            return [self performSelector:@selector(mod_boardBoardListItemFromPlist:) withObject:plist];
        };
        SWIZZLE_METHOD(object_getClass(ConcreteBoardListItem), boardBoardListItemFromPlist:, boardBoardListItemFromPlist_mod, "@@:@");
        
        BOOL (^canInitWithURL_mod)(id, NSURL*) = ^(id self, NSURL *URL) {
            if([URL.host hasSuffix:@".5ch.net"]) {
                NSString *newHost = [URL.host stringByReplacingOccurrencesOfString:@".5ch.net" withString:@".2ch.net"];
                URL = [[NSURL alloc] initWithScheme:URL.scheme host:newHost path:URL.path];
            }
            return ((BOOL (*)(id, SEL, NSURL*))objc_msgSend)(self, @selector(mod_canInitWithURL:), URL);
        };
        SWIZZLE_METHOD(object_getClass(SG2chErrorHandler), canInitWithURL:, canInitWithURL_mod, "c@:@");
    }
    
    BOOL (^customizeRequest)(id, NSMutableURLRequest*, id*) = ^(id self, NSMutableURLRequest *req, id* error) {
        BOOL ret = ((BOOL (*)(id, SEL, id, id*))objc_msgSend)(self, @selector(mod_customizeRequest:error:), req, error);
        [req setValue:@"api.5ch.net" forHTTPHeaderField:@"Host"];
        req.timeoutInterval = [[NSUserDefaults standardUserDefaults] doubleForKey:@"BSTAPIServerTimeout"];
        return ret;
    };
    SWIZZLE_METHOD(APIDownloader, customizeRequest:error:, customizeRequest, "c@:@:^id");
    
    id (^requestForSid_mod)(id, id) = ^(id self, id arg) {
        id req = ((id (*)(id, SEL, id))objc_msgSend)(self, @selector(mod_requestForSid:), arg);
        if ([req isKindOfClass:[NSMutableURLRequest class]]) {
            [req setTimeoutInterval:[[NSUserDefaults standardUserDefaults] doubleForKey:@"BSTAPIServerTimeout"]];
        }
        return req;
    };
    SWIZZLE_METHOD(JIMSidManager, requestForSid:, requestForSid_mod, "@@:@");
    
    void (^addCookies_mod)(id, NSString*, NSString*) = ^(id self, NSString *header, NSString *hostName) {
        static NSRegularExpression *sameSiteRegex;
        if(!sameSiteRegex) {
            sameSiteRegex = [[NSRegularExpression alloc] initWithPattern:@"( )?SameSite=[\\w]+(;)?" options:0 error:nil];
        }
        NSTextCheckingResult *match = [sameSiteRegex firstMatchInString:header options:0 range:NSMakeRange(0, header.length)];
        if(match) {
            header = [header stringByReplacingCharactersInRange:[match rangeAtIndex:0] withString:@""];
        }
        //NSLog(@"%@,%@",header,hostName);
        return ((void (*)(id, SEL, NSString*, NSString*))objc_msgSend)(self, @selector(mod_addCookies:fromServer:), header, hostName);
    };
    SWIZZLE_METHOD(CookieManager, addCookies:fromServer:, addCookies_mod, "v@:@:@");
    
    NSURL* (^settingTxtURL_mod)(id) = ^(id self) {
        NSURL *URL = [self performSelector:@selector(mod_settingTxtURL)];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL forceHTTPS = ([defaults integerForKey:@"BSTForce5chHTTPS"] == NSOnState);
        if([defaults integerForKey:@"BSTRoute2chTo5ch"] == NSOnState) {
            NSString *host = URL.host;
            if([host hasSuffix:@".2ch.net"]) {
                NSString *newHost = [host stringByReplacingOccurrencesOfString:@".2ch.net" withString:@".5ch.net"];
                URL = [[NSURL alloc] initWithScheme:@"https" host:newHost path:URL.path];
            }
        }
        if(forceHTTPS && ![URL.scheme isEqualToString:@"https"]) {
            NSString *host = URL.host;
            if([host hasSuffix:@".5ch.net"] || [host hasSuffix:@".2ch.net"] || [host hasSuffix:@".bbspink.com"]) {
                URL = [[NSURL alloc] initWithScheme:@"https" host:host path:URL.path];
            }
        }
        return URL;
    };
    SWIZZLE_METHOD(SettingTxtDetector, settingTxtURL, settingTxtURL_mod, "@@");
    
    NSURLRequest* (^requestForDownloadingHeadTxt_mod)(id) = ^(id self) {
        NSURLRequest *request = [self performSelector:@selector(mod_requestForDownloadingHeadTxt)];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL forceHTTPS = ([defaults integerForKey:@"BSTForce5chHTTPS"] == NSOnState);
        if([defaults integerForKey:@"BSTRoute2chTo5ch"] == NSOnState) {
            NSString *host = request.URL.host;
            if([host hasSuffix:@".2ch.net"]) {
                NSString *newHost = [host stringByReplacingOccurrencesOfString:@".2ch.net" withString:@".5ch.net"];
                NSURL *newURL = [[NSURL alloc] initWithScheme:@"https" host:newHost path:request.URL.path];
                NSMutableURLRequest *newRequest = [request mutableCopy];
                newRequest.URL = newURL;
                request = newRequest;
            }
        }
        if(forceHTTPS && ![request.URL.scheme isEqualToString:@"https"]) {
            NSString *host = request.URL.host;
            if([host hasSuffix:@".5ch.net"] || [host hasSuffix:@".2ch.net"] || [host hasSuffix:@".bbspink.com"]) {
                NSURL *newURL = [[NSURL alloc] initWithScheme:@"https" host:host path:request.URL.path];
                NSMutableURLRequest *newRequest = [request mutableCopy];
                newRequest.URL = newURL;
                request = newRequest;
            }
        }
        return request;
    };
    SWIZZLE_METHOD(LocalRulesCollector, requestForDownloadingHeadTxt, requestForDownloadingHeadTxt_mod, "@@");
    
    id (^initWithURL_mod2)(id, NSURL*) = ^(id self, NSURL *URL) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL forceHTTPS = ([defaults integerForKey:@"BSTForce5chHTTPS"] == NSOnState);
        if([defaults integerForKey:@"BSTRoute2chTo5ch"] == NSOnState) {
            NSString *host = URL.host;
            if([host hasSuffix:@".2ch.net"]) {
                if([URL.path.lastPathComponent isEqualToString:@"subject.txt"]) {
                    NSString *newHost = [host stringByReplacingOccurrencesOfString:@".2ch.net" withString:@".5ch.net"];
                    URL = [[NSURL alloc] initWithScheme:@"https" host:newHost path:URL.path];
                }
            }
        }
        if(forceHTTPS && ![URL.scheme isEqualToString:@"https"]) {
            NSString *host = URL.host;
            if([host hasSuffix:@".5ch.net"] || [host hasSuffix:@".2ch.net"] || [host hasSuffix:@".bbspink.com"]) {
                URL = [[NSURL alloc] initWithScheme:@"https" host:host path:URL.path];
            }
        }
        id obj = ((id (*)(id, SEL, id))objc_msgSend)(self, @selector(initWithURL_mod:), URL);
        return obj;
    };
    SWIZZLE_METHOD2(DownloadTask, initWithURL:, initWithURL_mod:, initWithURL_mod2, "@@:@");
    
    id (^initClusterWithURL_mod)(id, NSURL*, NSDictionary*) = ^(id self, NSURL *URL, NSDictionary *properties) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL forceHTTPS = ([defaults integerForKey:@"BSTForce5chHTTPS"] == NSOnState);
        BOOL isBbsCgi2ch = NO;
        if([defaults integerForKey:@"BSTRoute2chTo5ch"] == NSOnState) {
            NSRange range = [URL.absoluteString rangeOfString:@".2ch.net/test/bbs.cgi"];
            if(range.location != NSNotFound) {
                NSString *newHost = [URL.host stringByReplacingOccurrencesOfString:@".2ch.net" withString:@".5ch.net"];
                URL = [[NSURL alloc] initWithScheme:@"https" host:newHost path:URL.path];
                isBbsCgi2ch = YES;
            }
        }
        if(forceHTTPS && ![URL.scheme isEqualToString:@"https"]) {
            NSString *host = URL.host;
            if([host hasSuffix:@".5ch.net"] || [host hasSuffix:@".2ch.net"] || [host hasSuffix:@".bbspink.com"]) {
                URL = [[NSURL alloc] initWithScheme:@"https" host:host path:URL.path];
            }
        }
        if([properties isKindOfClass:[NSMutableDictionary class]]) {
            if(isBbsCgi2ch) {
                NSString *referer = properties[@"Referer"];
                if(referer) {
                    NSString *newReferer = [referer stringByReplacingOccurrencesOfString:@".2ch.net/" withString:@".5ch.net/"];
                    [(NSMutableDictionary *)properties setObject:newReferer forKey:@"Referer"];
                }
            }
            BST5chEnabler *enabler = [BST5chEnabler sharedInstance];
            [enabler reloadHeaderModificationRules];
            for(NSString *key in _headerModificationRules.allKeys) {
                id value = _headerModificationRules[key];
                if(![value isKindOfClass:[NSString class]]) continue;
                [(NSMutableDictionary *)properties setObject:value forKey:key];
            }
        }
        id obj = ((id (*)(id, SEL, NSURL*, NSDictionary*))objc_msgSend)(self, @selector(initClusterWithURL_mod:additionalProperties:), URL, properties);
        return obj;
    };
    SWIZZLE_METHOD2(SG2chConnector, initClusterWithURL:additionalProperties:, initClusterWithURL_mod:additionalProperties:, initClusterWithURL_mod, "@@:@:@");
    
    void (^receiveCookiesWithResponse_mod)(id, NSHTTPURLResponse *) = ^(id self, NSHTTPURLResponse *response) {
        if([[NSUserDefaults standardUserDefaults] integerForKey:@"BSTRoute2chTo5ch"] == NSOnState) {
            NSURL *URL = response.URL;
            NSURL *originalURL = [self performSelector:@selector(targetURL)];
            if([originalURL.host hasSuffix:@".2ch.net"] && [URL.host hasSuffix:@".5ch.net"]) {
                NSMutableDictionary *newHeaders = [response.allHeaderFields mutableCopy];
                NSString *cookie = newHeaders[@"Set-Cookie"];
                if(cookie) {
                    cookie = [cookie stringByReplacingOccurrencesOfString:@".5ch.net" withString:@".2ch.net"];
                    newHeaders[@"Set-Cookie"] = cookie;
                }
                NSString *newHost = [URL.host stringByReplacingOccurrencesOfString:@".5ch.net" withString:@".2ch.net"];
                URL = [[NSURL alloc] initWithScheme:URL.scheme host:newHost path:URL.path];
                response = [[NSHTTPURLResponse alloc] initWithURL:URL statusCode:response.statusCode HTTPVersion:@"HTTP/1.1" headerFields:newHeaders];
            }
        }
        [self performSelector:@selector(mod_receiveCookiesWithResponse:) withObject:response];
    };
    SWIZZLE_METHOD(ReplyMessenger, receiveCookiesWithResponse:, receiveCookiesWithResponse_mod, "v@:@");
    
    if(_bundleVersion < 1065) {
        BOOL (^detectMovedBoardWithResponseHTML_mod)(id, NSString*, NSString*) = ^(id self, NSString *contents, NSString *boardName) {
            NSString *(^newBoardURLString)(NSString *) = ^(NSString *oldURL) {
                NSHTTPURLResponse *resp = nil;
                NSError *error = nil;
                NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:oldURL]];
                req.HTTPMethod = @"HEAD";
                [NSURLConnection sendSynchronousRequest:req returningResponse:&resp error:&error];
                if(!error && resp.statusCode == 200) {
                    return [NSString stringWithFormat:@"http://%@%@/",[resp.URL.host stringByReplacingOccurrencesOfString:@".5ch.net" withString:@".2ch.net"],resp.URL.path];
                }
                return (NSString *)nil;
            };
            NSURL *oldURL = [self performSelector:@selector(URLForBoardName:) withObject:boardName];
            if([oldURL.host isEqualToString:@"5ch.net"]) {
                NSString *newLocation = newBoardURLString(oldURL.absoluteString);
                if(newLocation) {
                    contents = [NSString stringWithFormat:@"<a href=\"%@\"></a>",newLocation];
                }
            } else {
                NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"<a href=\"(.*)\".*>(.*)</a>" options:0 error:nil];
                NSTextCheckingResult *match = [regex firstMatchInString:contents options:0 range:NSMakeRange(0, contents.length)];
                if(match) {
                    NSString *urlStr = [contents substringWithRange:[match rangeAtIndex:1]];
                    NSRange range = [urlStr rangeOfString:@"https://5ch.net/"];
                    if(range.location != NSNotFound) {
                        NSString *newLocation = newBoardURLString(urlStr);
                        if(newLocation) contents = [contents stringByReplacingCharactersInRange:[match rangeAtIndex:1] withString:newLocation];
                    }
                }
            }
            return ((BOOL (*)(id, SEL, NSString*, NSString*))objc_msgSend)(self, @selector(mod_detectMovedBoardWithResponseHTML:boardName:), contents, boardName);
        };
        SWIZZLE_METHOD(BoardManager, detectMovedBoardWithResponseHTML:boardName:, detectMovedBoardWithResponseHTML_mod, "c@:@:@");
        
        NSDate* (^expiresDate_mod)(id) = ^(id self) {
            NSDate *date = [self performSelector:@selector(mod_expiresDate)];
            if(date) {
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSDateComponents *comps = [calendar components:NSYearCalendarUnit fromDate:date];
                if(comps.year < 1000) {
                    static NSDateFormatter *formatter = nil;
                    if(!formatter) {
                        formatter = [[[self class] performSelector:@selector(cookieDateFormatter)] copy];
                        formatter.dateFormat = @"EEEE, dd-MMM-yy HH:mm:ss 'GMT'";
                    }
                    date = [formatter dateFromString:[self performSelector:@selector(expires)]];
                }
            }
            return date;
        };
        SWIZZLE_METHOD(Cookie, expiresDate, expiresDate_mod, "@@");
    } else {
        BOOL (^detectMovedBoardWithResponseHTML_mod)(id, NSString*, NSURL*, NSString*) = ^(id self, NSString *contents, id oldURL, NSString *boardName) {
            if(!contents) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSAlert *alert = [[NSAlert alloc] init];
                    alert.messageText = @"スレッド一覧が表示できません";
                    alert.informativeText = [NSString stringWithFormat:@"掲示板 %@ のURL (%@) が正しくない可能性があります。",boardName,oldURL];
                    [alert runModal];
                });
                return NO;
            }
            return ((BOOL (*)(id, SEL, NSString*, NSURL*, NSString*))objc_msgSend)(self, @selector(mod_detectMovedBoardWithResponseHTML:oldURL:boardName:), contents, oldURL, boardName);
        };
        SWIZZLE_METHOD(BoardManager, detectMovedBoardWithResponseHTML:oldURL:boardName:, detectMovedBoardWithResponseHTML_mod, "c@:@:@:@");
    }
    
    self.status = BSTPluginStatusEnabled;
}

@end
