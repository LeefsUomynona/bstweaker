//
//  BSTThreadListNGFilter.m
//  BSTweaker
//
//  Created by anonymous on 2020/05/02.
//  Copyright © 2020年 anonymous. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Cocoa/Cocoa.h>
#import "BSTPlugIns.h"

@interface BSTThreadListNGFilter ()
- (BOOL)isNGThread:(NSString *)threadTitle forBoard:(NSString *)boardName;
@property(nonatomic, readwrite) BSTPluginStatus status;
@property(nonatomic, copy) NSString *plistPath;
@property(nonatomic, strong) NSMutableDictionary *rules;
@property(nonatomic, strong) NSMutableArray *wildcardRules;
@property(nonatomic, strong) NSDate *ruleModificationDate;
@end

@implementation BSTThreadListNGFilter

+ (id)sharedInstance
{
    static BSTThreadListNGFilter* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTThreadListNGFilter alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if(self) {
        _plistPath = [[[[[NSBundle bundleForClass:[BSTThreadListAppearanceChanger class]] bundlePath] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"BSTweaker-NGThread.plist"];
        _rules = [[NSMutableDictionary alloc] init];
        _wildcardRules = [[NSMutableArray alloc] init];
        NSInteger ret = [self reloadRules];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BSTThreadListNGFilterRuleUpdated" object:self userInfo:@{@"RulesCount":@(ret)}];
        });
    }
    return self;
}

- (NSInteger)reloadRules
{
    NSMutableDictionary *plistDic = [[NSMutableDictionary alloc] initWithContentsOfFile:_plistPath];
    if(!plistDic) {
        [_rules removeAllObjects];
        [_wildcardRules removeAllObjects];
        return 0;
    }
    NSDictionary *plistAttr = [[NSFileManager defaultManager] attributesOfItemAtPath:_plistPath error:nil];
    NSDate *modificationDate = [plistAttr objectForKey:NSFileModificationDate];
    if(_ruleModificationDate && [_ruleModificationDate compare:modificationDate] != NSOrderedAscending) {
        return -1;
    }
    self.ruleModificationDate = modificationDate;
    [_rules removeAllObjects];
    [_wildcardRules removeAllObjects];
    
    NSArray *allKeys = [plistDic allKeys];
    NSUInteger rulesCount = 0;
    for(NSString *key in allKeys) {
        NSArray *rules = [plistDic objectForKey:key];
        if(![rules isKindOfClass:[NSArray class]]) continue;
        NSMutableArray *rulesForBoard = [[NSMutableArray alloc] init];
        for(NSDictionary *rule in rules) {
            if(![rule isKindOfClass:[NSDictionary class]]) continue;
            NSString *regexStr = [rule objectForKey:@"keyword"];
            NSUInteger options = [[rule objectForKey:@"options"] unsignedIntegerValue];
            if(!regexStr) continue;
            NSUInteger regexOptions = 0;
            if(options & 1) {
                regexOptions |= NSRegularExpressionCaseInsensitive;
            }
            NSRegularExpression *regex = [[NSRegularExpression alloc] initWithPattern:regexStr options:regexOptions error:nil];
            if(!regex) continue;
            [rulesForBoard addObject:regex];
        }
        if([key isEqualToString:@"*"]) {
            [_wildcardRules addObjectsFromArray:rulesForBoard];
        } else {
            [_rules setObject:rulesForBoard forKey:key];
        }
        rulesCount += rulesForBoard.count;
    }
    return rulesCount;
}

- (BOOL)isNGThread:(NSString *)threadTitle forBoard:(NSString *)boardName
{
    NSRange range = NSMakeRange(0, threadTitle.length);
    for(NSRegularExpression *regex in _wildcardRules) {
        NSRange match = [regex rangeOfFirstMatchInString:threadTitle options:0 range:range];
        if(match.location != NSNotFound) {
            return YES;
        }
    }
    NSArray *rulesForBoard = [_rules objectForKey:boardName];
    if(rulesForBoard) {
        for(NSRegularExpression *regex in rulesForBoard) {
            NSRange match = [regex rangeOfFirstMatchInString:threadTitle options:0 range:range];
            if(match.location != NSNotFound) {
                return YES;
            }
        }
    }
    return NO;
}

- (void)swizzle
{
    Class ThreadList = objc_lookUpClass("BSDBThreadList");
    if(!ThreadList) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    
    void (^setFilteredThreads_mod)(id, NSArray*) = ^(id self, NSArray *threads) {
        NSString *boardName = [self performSelector:@selector(boardName)];
        if(threads) {
            BSTThreadListNGFilter *filter = [BSTThreadListNGFilter sharedInstance];
            NSInteger ret = [filter reloadRules];
            if(ret >= 0) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"BSTThreadListNGFilterRuleUpdated" object:self userInfo:@{@"RulesCount":@(ret)}];
            }
            NSMutableArray *filtered = [[NSMutableArray alloc] init];
            for(id thread in threads) {
                NSString *title = [thread performSelector:@selector(threadName)];
                if([filter isNGThread:title forBoard:boardName]) continue;
                [filtered addObject:thread];
            }
            threads = filtered;
        }
        ((void (*)(id, SEL, NSArray*))objc_msgSend)(self, @selector(mod_setFilteredThreads:), threads);
    };
    SWIZZLE_METHOD(ThreadList, setFilteredThreads:, setFilteredThreads_mod, "v@:@");
    
    self.status = BSTPluginStatusEnabled;
}

@end
