//
//  BSTBrowserSelector.m
//  BSTweaker
//
//  Created by anonymous on 2019/02/16.
//  Copyright © 2019年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BSTPlugIns.h"

@interface BSTBrowserSelector ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@end

@implementation BSTBrowserSelector

+ (id)sharedInstance
{
    static BSTBrowserSelector* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTBrowserSelector alloc] init];
    });
    return sharedInstance;
}

- (void)swizzle
{
    NSURL* (^URLForDefaultWebBrowser_mod)(id) = ^(id self) {
        NSString *path = [[NSUserDefaults standardUserDefaults] objectForKey:@"BSTPreferredBrowser"];
        return [NSURL fileURLWithPath:path];
    };
    SWIZZLE_METHOD([NSWorkspace class], URLForDefaultWebBrowser, URLForDefaultWebBrowser_mod, "@@");
    
    /* ugly hack for built-in image previewer */
    BOOL (^openURLs_mod)(id, id, NSString *, NSUInteger, id, id*) = ^(id self, id urls, NSString *bundleIdentifier, NSUInteger options, id descriptor, id *identifiers) {
        if(!bundleIdentifier) {
            NSArray *callstack = [NSThread callStackSymbols];
            if(callstack.count > 1) {
                NSMutableArray *tokens = [[callstack[1] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] mutableCopy];
                [tokens removeObject:@""];
                if([tokens[1] isEqualToString:@"ImagePreviewer"]) {
                    NSString *path = [[NSUserDefaults standardUserDefaults] objectForKey:@"BSTPreferredBrowser"];
                    NSBundle *bundle = [NSBundle bundleWithPath:path];
                    bundleIdentifier = [bundle bundleIdentifier];
                }
            }
        }
        return ((BOOL (*)(id, SEL, id, id, NSUInteger, id, id*))objc_msgSend)(self, @selector(mod_openURLs:withAppBundleIdentifier:options:additionalEventParamDescriptor:launchIdentifiers:), urls, bundleIdentifier, options, descriptor, identifiers);
    };
    SWIZZLE_METHOD([NSWorkspace class], openURLs:withAppBundleIdentifier:options:additionalEventParamDescriptor:launchIdentifiers:, openURLs_mod, "c@:@:@:@:@:^@");
    
    self.status = BSTPluginStatusEnabled;
}

@end
