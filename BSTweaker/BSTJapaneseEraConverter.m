//
//  BSTJapaneseEraConverter.m
//  BSTweaker
//
//  Created by anonymous on 2019/04/16.
//  Copyright © 2019年 anonymous. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Cocoa/Cocoa.h>
#import "BSTPlugIns.h"

@interface BSTJapaneseEraConverter ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@end

@implementation BSTJapaneseEraConverter

+ (id)sharedInstance
{
    static BSTJapaneseEraConverter* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTJapaneseEraConverter alloc] init];
    });
    return sharedInstance;
}

- (void)swizzle
{
    Class CMXTextParser = objc_lookUpClass("CMXTextParser");
    if(!CMXTextParser) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    
    id (^messageWithDATLine_mod)(id, NSArray*) = ^(id self, NSArray *components) {
        NSString *dateString = [components objectAtIndex:2];
        //NSLog(@"olddate: %@",dateString);
        const char *ptr = [dateString UTF8String];
        if(ptr && (strstr(ptr, "平成") || strstr(ptr, "令和"))) {
            char *newDate = strdup(ptr);
            char *newPtr = newDate;
            int state = 0;
            for(;*ptr;ptr++) {
                if(state == 0) {
                    if(*ptr == '<') {
                        state = 1;
                        continue;
                    }
                    *newPtr++ = *ptr;
                }
                else if(*ptr == '>') state = 0;
            }
            *newPtr = 0;
            newPtr = newDate;
            int year = 0;
            while(isspace(*newPtr)) newPtr++;
            if(!strncmp(newPtr, "平成", strlen("平成"))) {
                year = 1989;
                newPtr += strlen("平成");
            }
            else if(!strncmp(newPtr, "令和", strlen("令和"))) {
                year = 2019;
                newPtr += strlen("令和");
            }
            if(year) {
                while(isspace(*newPtr)) newPtr++;
                if(!strncmp(newPtr, "元年", strlen("元年"))) {
                    newPtr += strlen("元年");
                }
                else {
                    year += strtoul(newPtr, &newPtr, 10) - 1;
                }
                while(*newPtr < '0' || *newPtr > '9') newPtr++;
                
                NSString *newDateString = [NSString stringWithFormat:@"%d/%@",year,[NSString stringWithUTF8String:newPtr]];
                NSMutableArray *newComponents = [NSMutableArray arrayWithArray:components];
                [newComponents replaceObjectAtIndex:2 withObject:newDateString];
                components = newComponents;
                //NSLog(@"newdate: %@",newDateString);
            }
            free(newDate);
        }
        return ((id (*)(id, SEL, NSArray*))objc_msgSend)(CMXTextParser, @selector(mod_messageWithDATLineComponentsSeparatedByNewline:), components);
    };
    SWIZZLE_METHOD(object_getClass(CMXTextParser), messageWithDATLineComponentsSeparatedByNewline:, messageWithDATLine_mod, "@@:@");
    
    self.status = BSTPluginStatusEnabled;
}

@end
