//
//  BSTReplyNotifier.m
//  BSTweaker
//
//  Created by anonymous on 2017/12/06.
//  Copyright © 2017年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BSTPlugIns.h"

static NSRegularExpression *regex;
extern NSColor *BSTReplyNotifierBGColor;
extern NSColor *BSTReplyNotifierAltBGColor;

@interface BSTReplyNotifier ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@property(nonatomic, strong) NSMutableSet *myResSet;
@end

@implementation BSTReplyNotifier

+ (id)sharedInstance
{
    static BSTReplyNotifier* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTReplyNotifier alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if(self) {
        _myResSet = [[NSMutableSet alloc] init];
    }
    return self;
}

- (void)swizzle
{
    if(_status != BSTPluginStatusDisabled) return;
    
    regex = [[NSRegularExpression alloc] initWithPattern:@"(&gt;|\\uff1e)+([\\d]+)[^-]" options:0 error:nil];
    Class ReplyMessenger = objc_lookUpClass("CMRReplyMessenger");
    Class ThreadLayout = objc_lookUpClass("CMRThreadLayout");
    Class ThreadTextDownloader = objc_lookUpClass("ThreadTextDownloader");
    Class ThreadPlistComposer = objc_lookUpClass("CMRThreadPlistComposer");
    Class ThreadDictReader = objc_lookUpClass("CMRThreadDictReader");
    Class AttributedMessageComposer = objc_lookUpClass("CMRAttributedMessageComposer");
    Class DATReader = objc_lookUpClass("CMR2chDATReader");
    Class ThreadView = objc_lookUpClass("CMRThreadView");
    if(!ReplyMessenger||!ThreadLayout||!ThreadTextDownloader||!ThreadPlistComposer||!ThreadDictReader||!AttributedMessageComposer||!DATReader) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    
    void (^connectorResourceDidFinishLoading_mod)(id, id) = ^(id self, id sender) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)[sender response];
        id res = [[response allHeaderFields] objectForKey:@"x-Resnum"];
        if(res) {
            NSString *board = [self performSelector:@selector(boardName)];
            NSString *key = [self performSelector:@selector(formItemKey)];
            NSString *identifier = [NSString stringWithFormat:@"%@/%@/%d",board,key,[res intValue]];
            //NSLog(@"%@",identifier);
            BSTReplyNotifier *notifier = [BSTReplyNotifier sharedInstance];
            [notifier commitReplyWithIdentifier:identifier];
        }
        //NSLog(@"%@",[response description]);
        [self performSelector:@selector(mod_connectorResourceDidFinishLoading:) withObject:sender];
    };
    SWIZZLE_METHOD(ReplyMessenger, connectorResourceDidFinishLoading:, connectorResourceDidFinishLoading_mod, "v@:@");
    
#if 0
    void (^mergeComposingResult_mod)(id, id) = ^(id self, id operation) {
        if([NSStringFromClass([operation class]) isEqualToString:@"BSThreadComposingOperation"]) {
            if(![operation isCancelled]) {
                id messageBuffer = [operation performSelector:@selector(messageBuffer)];
                NSUInteger currentMessageCount = [[self performSelector:@selector(allMessages)] count];
                NSUInteger newMessageCount = [messageBuffer count];
                //NSLog(@"currently have: %ld, new: %ld ",currentMessageCount,newMessageCount);
                if(newMessageCount) {
                    id signature = [operation performSelector:@selector(signature)];
                    NSString *board = [signature performSelector:@selector(boardName)];
                    NSString *key = [signature performSelector:@selector(identifier)];
                    NSArray *messages = [messageBuffer performSelector:@selector(messages)];
                    unsigned int n = 1;
                    for(id message in messages) {
                        NSString *identifier = [NSString stringWithFormat:@"%@/%@/%lu",board,key,currentMessageCount+n];
                        //NSLog(@"%@",identifier);
                        BSTReplyNotifier *notifier = [BSTReplyNotifier sharedInstance];
                        if([notifier isMyReply:identifier]) {
                            //NSLog(@"This is my response!");
                            objc_setAssociatedObject(message, "MESSAGE_ATTRIBUTE_MY_RESPONSE", @YES, OBJC_ASSOCIATION_RETAIN);
                            NSColor *color = nil;
                            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                            if([defaults integerForKey:@"BSTReplyNotifierShouldChangeBGColor"] == NSOnState) {
                                NSData *colorData = [[NSUserDefaults standardUserDefaults] dataForKey:@"BSTReplyNotifierBGColor"];
                                if(colorData) color = (NSColor *)[NSUnarchiver unarchiveObjectWithData:colorData];
                            }
                            if(color) {
                                id ranges =  [operation performSelector:@selector(rangeBuffer)];
                                NSRange range = ((NSRange (*)(id, SEL, NSUInteger))objc_msgSend)(ranges, @selector(rangeAtIndex:), n-1);
                                NSMutableAttributedString *attrStr = [operation performSelector:@selector(attrStrBuffer)];
                                [attrStr addAttribute:NSBackgroundColorAttributeName value:color range:range];
                            }
                        }
                        n++;
                    }
                }
            }
        }
        [self performSelector:@selector(mod_mergeComposingResult:) withObject:operation];
        
    };
    SWIZZLE_METHOD(ThreadLayout, mergeComposingResult:, mergeComposingResult_mod, "v@:@");
#else
    BOOL (^composeNextMessageWithComposer_mod2)(id, id) = ^(id self, id composer) {
        BOOL ret = ((BOOL (*)(id, SEL, id))objc_msgSend)(self, @selector(mod_composeNextMessageWithComposer:), composer);
        if(ret && [NSStringFromClass([composer class]) isEqualToString:@"CMRThreadMessageBuffer"]) {
            id signature = nil;
            id contents = [self performSelector:@selector(fileContents)];
            if(contents) {
                signature = objc_getAssociatedObject(contents, "THREAD_SIGNATURE");
            }
            if(signature) {
                NSString *board = [signature performSelector:@selector(boardName)];
                NSString *key = [signature performSelector:@selector(identifier)];
                NSUInteger index = ((NSUInteger (*)(id, SEL))objc_msgSend)(self, @selector(nextMessageIndex));
                NSString *identifier = [NSString stringWithFormat:@"%@/%@/%lu",board,key,index];
                //NSLog(@"%@",identifier);
                id message = [composer performSelector:@selector(lastMessage)];
                BSTReplyNotifier *notifier = [BSTReplyNotifier sharedInstance];
                if([notifier isMyReply:identifier]) {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    //NSLog(@"This is my response!");
                    objc_setAssociatedObject(message, "MESSAGE_ATTRIBUTE_MY_RESPONSE", @YES, OBJC_ASSOCIATION_RETAIN);
                    if([defaults integerForKey:@"BSTReplyNotifierShouldBookmarkMyReply"] == NSOnState) {
                        ((void (*)(id, SEL, BOOL))objc_msgSend)(message, @selector(setHasBookmark:), YES);
                    }
                    objc_removeAssociatedObjects(contents);
                }
            }
        }
        return ret;
    };
    SWIZZLE_METHOD(DATReader, composeNextMessageWithComposer:, composeNextMessageWithComposer_mod2, "c@:@");
#endif
    
    NSDictionary * (^dictionaryByAppendingContents_mod)(id, NSString*, NSUInteger) = ^(id self, NSString *datContents, NSUInteger aLength) {
        NSDictionary *local = [self performSelector:@selector(localThreadsDict)];
        NSDictionary *dic = ((NSDictionary * (*)(id, SEL, NSString*, NSUInteger))objc_msgSend)(self, @selector(mod_dictionaryByAppendingContents:dataLength:), datContents, aLength);
        NSArray *messages = [local objectForKey:@"Contents"];
        NSArray *newMessages = [dic objectForKey:@"Contents"];
        NSUInteger currentMessageCount = [messages count];
        NSUInteger newMessageCount = [newMessages count];
        int replyCount = 0;
        NSUInteger resNumber = 0;
        //NSLog(@"dict: %lu,%lu",currentMessageCount,newMessageCount);
        if(currentMessageCount < newMessageCount) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            BOOL shouldNotifyMessage = [defaults integerForKey:@"BSTReplyNotifierShouldSendNotification"] == NSOnState;
            NSUInteger i;
            id signature = [self performSelector:@selector(threadSignature)];
            NSString *board = [signature performSelector:@selector(boardName)];
            NSString *key = [signature performSelector:@selector(identifier)];
            for(i=currentMessageCount;i<newMessageCount;i++) {
                id message = [newMessages objectAtIndex:i];
                NSString *identifier = [NSString stringWithFormat:@"%@/%@/%lu",board,key,i+1];
                //NSLog(@"%@",identifier);
                BSTReplyNotifier *notifier = [BSTReplyNotifier sharedInstance];
                if([notifier isMyReply:identifier]) {
                    [message setObject:@YES forKey:@"MyResponse"];
                    objc_setAssociatedObject(datContents, "THREAD_SIGNATURE", signature, OBJC_ASSOCIATION_RETAIN);
                    if([defaults integerForKey:@"BSTReplyNotifierShouldBookmarkMyReply"] == NSOnState) {
                        UInt32 status = [[message objectForKey:@"Status"] unsignedIntValue];
                        status |= 0x1000;
                        [message setObject:@(status) forKey:@"Status"];
                    }
                    //NSLog(@"This is my response!");
                }
                if(shouldNotifyMessage) {
                    NSString *messageTxt = [message objectForKey:@"Message"];
                    if(message) {
                        NSArray *matches = [regex matchesInString:messageTxt options:0 range:NSMakeRange(0, [messageTxt length])];
                        for(NSTextCheckingResult *result in matches) {
                            int n = [[messageTxt substringWithRange:[result rangeAtIndex:2]] intValue];
                            //NSLog(@"%d",n);
                            if(n>0 && n<newMessageCount+1) {
                                id target = [newMessages objectAtIndex:n-1];
                                if([[target objectForKey:@"MyResponse"] boolValue]) {
                                    //NSLog(@"This is a reply to my response!");
                                    replyCount++;
                                    resNumber = i+1;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            if(replyCount) {
                //NSLog(@"sending notification...");
                NSUserNotificationCenter *nc = [NSUserNotificationCenter defaultUserNotificationCenter];
                nc.delegate = (id <NSUserNotificationCenterDelegate>)[BSTReplyNotifier sharedInstance];
                NSUserNotification * newUserNotification = [[NSUserNotification alloc] init];
                newUserNotification.title = @"新着の返信メッセージ";
                if(replyCount == 1)
                    newUserNotification.informativeText = [NSString stringWithFormat:@"スレ %@ のレス番 %lu があなたの書き込みにレスしました",[dic objectForKey:@"Title"],resNumber];
                else
                    newUserNotification.informativeText = [NSString stringWithFormat:@"スレ %@ にあなたの書き込みへのレスが %d 件あります",[dic objectForKey:@"Title"],replyCount];
                newUserNotification.hasActionButton = NO;
                //newUserNotification.identifier = key;
                //[nc removeDeliveredNotification:newUserNotification];
                [nc deliverNotification:newUserNotification];
                [[NSSound soundNamed:[defaults stringForKey:@"BSTNotificationSound"]] play];
            }
        }
        return dic;
    };
    SWIZZLE_METHOD(ThreadTextDownloader, dictionaryByAppendingContents:dataLength:, dictionaryByAppendingContents_mod, "@@:@:Q");
    
    void (^concludeComposing_mod)(id, id) = ^(id self, id aMessage) {
        id myRespFlag = objc_getAssociatedObject(aMessage, "MESSAGE_ATTRIBUTE_MY_RESPONSE");
        if([myRespFlag boolValue]) {
            //NSLog(@"MyResponse key found for message object %@",aMessage);
            ((void (*)(id, SEL, id, NSString*))objc_msgSend)(self, @selector(addNewEntry:forKey:), @YES, @"MyResponse");
        }
        [self performSelector:@selector(mod_concludeComposing:) withObject:aMessage];
    };
    SWIZZLE_METHOD(ThreadPlistComposer, concludeComposing:, concludeComposing_mod, "v@:@");
    
    BOOL (^composeNextMessageWithComposer_mod)(id, id) = ^(id self, id composer) {
        //NSLog(@"composeNextMessageWithComposer,%@",NSStringFromClass([composer class]));
        BOOL ret = (BOOL)[self performSelector:@selector(mod_composeNextMessageWithComposer:) withObject:composer];
        if(ret && [NSStringFromClass([composer class]) isEqualToString:@"CMRThreadMessageBuffer"]) {
            NSArray *ary = [self performSelector:@selector(messageDictArray)];
            NSUInteger idx  = (NSUInteger)[self performSelector:@selector(nextMessageIndex)];
            NSDictionary *messageDict = [ary objectAtIndex:idx-1];
            if([[messageDict objectForKey:@"MyResponse"] boolValue]) {
                //NSLog(@"MyResponse key found for dict %@",messageDict);
                id message = [composer performSelector:@selector(lastMessage)];
                objc_setAssociatedObject(message, "MESSAGE_ATTRIBUTE_MY_RESPONSE", @YES, OBJC_ASSOCIATION_RETAIN);
            }
        }
        return ret;
    };
    SWIZZLE_METHOD(ThreadDictReader, composeNextMessageWithComposer:, composeNextMessageWithComposer_mod, "c@:@");
    
    void (^composeIndex_mod)(id, id) = ^(id self, id aMessage) {
        id myRespFlag = objc_getAssociatedObject(aMessage, "MESSAGE_ATTRIBUTE_MY_RESPONSE");
        if([myRespFlag boolValue]) {
            NSMutableAttributedString *attrStr = [self performSelector:@selector(contentsStorage)];
            NSRange range;
            range.location = attrStr.length;
            [self performSelector:@selector(mod_composeIndex:) withObject:aMessage];
            range.length = attrStr.length - range.location;
            if([[attrStr string] characterAtIndex:range.location+range.length-1] == ' ') {
                range.length--;
            }
            [attrStr addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:range];
        }
        else {
            [self performSelector:@selector(mod_composeIndex:) withObject:aMessage];
        }
    };
    SWIZZLE_METHOD(AttributedMessageComposer, composeIndex:, composeIndex_mod, "v@:@");
    
    void (^appendComposingAttrString_mod)(id, id) = ^(id self, id operation) {
        [self performSelector:@selector(mod_appendComposingAttrString:) withObject:operation];
        if([operation isCancelled]) return;
        if(BSTReplyNotifierBGColor || BSTReplyNotifierAltBGColor) {
            id messageBuffer = [operation performSelector:@selector(messageBuffer)];
            NSUInteger newMessageCount = [messageBuffer count];
            if(newMessageCount) {
                id ranges =  [self performSelector:@selector(messageRanges)];
                NSArray *allMessages = [self performSelector:@selector(allMessages)];
                NSUInteger currentMessageCount = [allMessages count];
                NSInteger messageIndex = currentMessageCount - newMessageCount;
                NSArray *messages = [messageBuffer performSelector:@selector(messages)];
                for(id message in messages) {
                    id myRespFlag = objc_getAssociatedObject(message, "MESSAGE_ATTRIBUTE_MY_RESPONSE");
                    if(BSTReplyNotifierBGColor && [myRespFlag boolValue]) {
                        //NSLog(@"My message at %ld",messageIndex+1);
                        NSRange range = ((NSRange (*)(id, SEL, NSUInteger))objc_msgSend)(ranges, @selector(rangeAtIndex:), messageIndex);
                        //[[self layoutManager] addTemporaryAttribute:NSBackgroundColorAttributeName value:BSTReplyNotifierBGColor forCharacterRange:range];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[self textStorage] addAttribute:NSBackgroundColorAttributeName value:BSTReplyNotifierBGColor range:range];
                        });
                    }
                    else if(BSTReplyNotifierAltBGColor) {
                        NSIndexSet *references = [message performSelector:@selector(referencingIndexes)];
                        [references enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
                            if(idx < currentMessageCount) {
                                id tmpMessage = allMessages[idx];
                                id myRespFlag = objc_getAssociatedObject(tmpMessage, "MESSAGE_ATTRIBUTE_MY_RESPONSE");
                                if([myRespFlag boolValue]) {
                                    NSRange range = ((NSRange (*)(id, SEL, NSUInteger))objc_msgSend)(ranges, @selector(rangeAtIndex:), messageIndex);
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [[self textStorage] addAttribute:NSBackgroundColorAttributeName value:BSTReplyNotifierAltBGColor range:range];
                                    });
                                    *stop = YES;
                                }
                            }
                        }];
                    }
                    messageIndex++;
                }
            }
        }
    };
    SWIZZLE_METHOD(ThreadLayout, appendComposingAttrString:, appendComposingAttrString_mod, "v@:@");
    
    void (^fillBackgroundRectArray_mod)(id, const NSRect *, NSUInteger, NSRange, NSColor *) = ^(id self, const NSRect *rectArray, NSUInteger rectCount, NSRange charRange, NSColor *color) {
        NSArray<NSTextContainer *> *containers = [self textContainers];
        if(containers && containers.count && [containers[0].textView isKindOfClass:ThreadView]) {
            if([color isEqual:BSTReplyNotifierBGColor] || [color isEqual:BSTReplyNotifierAltBGColor]) {
                NSUInteger i;
                NSRect unionRect = NSZeroRect;
                for(i=0;i<rectCount;i++) {
                    unionRect = NSUnionRect(unionRect, rectArray[i]);
                }
                unionRect.size.height += 4.0;
                unionRect.size.width += 4.0;
                unionRect.origin.x -= 2.0;
                unionRect.origin.y -= 2.0;
                NSBezierPath *path = [NSBezierPath bezierPathWithRoundedRect:unionRect xRadius:7 yRadius:7];
                [path fill];
                //((void (*)(id, SEL, const NSRect *, NSUInteger, NSRange, NSColor *))objc_msgSend)(self, @selector(mod_fillBackgroundRectArray:count:forCharacterRange:color:), &unionRect, 1, charRange, color);
                return;
            }
        }
        ((void (*)(id, SEL, const NSRect *, NSUInteger, NSRange, NSColor *))objc_msgSend)(self, @selector(mod_fillBackgroundRectArray:count:forCharacterRange:color:), rectArray, rectCount, charRange, color);
    };
    SWIZZLE_METHOD([NSLayoutManager class], fillBackgroundRectArray:count:forCharacterRange:color:, fillBackgroundRectArray_mod, "v@:^{CGRect={CGPoint=dd}{CGSize=dd}}:Q:{NSRange=QQ}:@");
    
    self.status = BSTPluginStatusEnabled;
}

- (void)commitReplyWithIdentifier:(NSString *)identifier
{
    [_myResSet addObject:identifier];
}

- (BOOL)isMyReply:(NSString *)identifier
{
    if([_myResSet containsObject:identifier]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [_myResSet removeObject:identifier];
        });
        return YES;
    }
    return NO;
}

- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center shouldPresentNotification:(NSUserNotification *)notification {
    return YES;
}

@end
