//
//  BSTBoardListHeightChanger.m
//  BSTweaker
//
//  Created by anonymous on 2017/12/06.
//  Copyright © 2017年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BSTPlugIns.h"

@interface BSTBoardListHeightChanger ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@end

@implementation BSTBoardListHeightChanger

+ (id)sharedInstance
{
    static BSTBoardListHeightChanger* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTBoardListHeightChanger alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if(self) {
        float bundleVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] floatValue];
        float shortVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] floatValue];
        if(bundleVersion < 1012 || shortVersion < 3.0f) {
            _status = BSTPluginStatusBlockedByVersion;
        }
    }
    return self;
}

- (void)swizzle
{
    if(_status != BSTPluginStatusDisabled) return;
    
    Class Browser = objc_lookUpClass("CMRBrowser");
    if(!Browser) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    
    void (^appDefaultsLayoutSettingsUpdated_mod)(id, NSNotification *) = ^(id self, NSNotification *aNotification) {
        [self performSelector:@selector(mod_appDefaultsLayoutSettingsUpdated:) withObject:aNotification];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSOutlineView	*boardListTable = [self performSelector:@selector(boardListTable)];
        [boardListTable setRowSizeStyle:[defaults integerForKey:@"BSRHCBoardListHeight"]];
    };
    SWIZZLE_METHOD(Browser, appDefaultsLayoutSettingsUpdated:, appDefaultsLayoutSettingsUpdated_mod, "v@:@");
    
    void (^setupBoardListTable_mod)(id) = ^(id self) {
        [self performSelector:@selector(mod_setupBoardListTable)];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSOutlineView	*boardListTable = [self performSelector:@selector(boardListTable)];
        [boardListTable setRowSizeStyle:[defaults integerForKey:@"BSRHCBoardListHeight"]];
    };
    SWIZZLE_METHOD(Browser, setupBoardListTable, setupBoardListTable_mod, "v@");
    
    self.status = BSTPluginStatusEnabled;
}

@end
