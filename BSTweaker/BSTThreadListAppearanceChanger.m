//
//  BSTThreadListAppearanceChanger.m
//  BSTweaker
//
//  Created by anonymous on 2017/12/04.
//  Copyright © 2017年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BSTPlugIns.h"

@implementation NSTextFieldCell (BSTVerticalAlignment)
#if MAC_OS_X_VERSION_MAX_ALLOWED < 101500
- (void)setVerticalCentering:(BOOL)centerVertical
{
    _cFlags.vCentered = centerVertical ? 1 : 0;
}
#else
- (NSRect)mod_titleRectForBounds:(NSRect)theRect
{
    /* get the standard text content rectangle */
    NSRect titleFrame = [self titleRectForBounds:theRect];
    
    /* put left/right padding */
    if (titleFrame.size.width > 4) {
        titleFrame.origin.x += 2;
        titleFrame.size.width -= 4;
    }
    
    /* find out how big the rendered text will be */
    NSAttributedString *attrString = self.attributedStringValue;
    NSRect textRect = [attrString boundingRectWithSize: titleFrame.size
                                               options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin ];
    
    /* If the height of the rendered text is less then the available height,
     * we modify the titleRect to center the text vertically */
    if (textRect.size.height < titleFrame.size.height) {
        titleFrame.origin.y = theRect.origin.y + (theRect.size.height - textRect.size.height) / 2.0;
        titleFrame.size.height = textRect.size.height;
    }
    return titleFrame;
}
#endif
@end

@interface BSTThreadListAppearanceChanger ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@property(nonatomic, readonly) float bundleVersion;
@property(nonatomic, readonly) float shortVersion;
@end

@implementation BSTThreadListAppearanceChanger

+ (id)sharedInstance
{
    static BSTThreadListAppearanceChanger* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTThreadListAppearanceChanger alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if(self) {
        _bundleVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] floatValue];
        _shortVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] floatValue];
        /*if(_bundleVersion < 1057 || _shortVersion < 3.0f) {
            _status = BSTPluginStatusBlockedByVersion;
        }*/
    }
    return self;
}

- (void)swizzle
{
    if(_status != BSTPluginStatusDisabled) return;
    
    Class AppDefaults = objc_lookUpClass("AppDefaults");
    Class ThreadList = objc_lookUpClass("BSDBThreadList");
    Class Browser = objc_lookUpClass("CMRBrowser");
    if(!ThreadList || !Browser) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    if(_bundleVersion >= 1057 && _shortVersion >= 3.0f && !AppDefaults) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    
    if(_bundleVersion >= 1057 && _shortVersion >= 3.0f) {
        double (^threadsListRowHeight_mod)(id) = ^(id self) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            double value = [defaults doubleForKey:@"BSRHCThreadListHeight"];
            //fprintf(stderr, "%f\n",value);
            return value;
        };
        SWIZZLE_METHOD(AppDefaults, threadsListRowHeight, threadsListRowHeight_mod, "d@");
        
        void (^setThreadsListFontSize_mod)(id, double) = ^(id self, double size) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setFloat:size forKey:@"BSTFontSize"];
            ((void (*)(id, SEL, double))objc_msgSend)(self, @selector(mod_setThreadsListFontSize:), size);
        };
        SWIZZLE_METHOD(AppDefaults, setThreadsListFontSize:, setThreadsListFontSize_mod, "v@:d");
        
#if 0
        NSFont* (^threadsListFont_mod)(id) = ^(id self) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSFont *font = [NSFont fontWithName:[defaults stringForKey:@"BSTFontName"] size:[defaults floatForKey:@"BSTFontSize"]];
            if(!font) font = [NSFont systemFontOfSize:[defaults floatForKey:@"BSTFontSize"]];
            else if([font.fontName isEqualToString:@".SFNSText"] || [font.fontName isEqualToString:@".SFNSDisplay"]) {
                font = [NSFont systemFontOfSize:font.pointSize];
            }
            return font;
        };
        SWIZZLE_METHOD(AppDefaults, threadsListFont, threadsListFont_mod, "@@");
        SWIZZLE_METHOD(AppDefaults, threadsListNewThreadFont, threadsListFont_mod, "@@");
        SWIZZLE_METHOD(AppDefaults, threadsListDatOchiThreadFont, threadsListFont_mod, "@@");
#endif
        
        id (^objectValueForTableColumn_mod)(id, NSTableView *, NSTableColumn *, NSInteger) = ^(id self, NSTableView * aTableView, NSTableColumn *aTableColumn, NSInteger rowIndex) {
            id obj = ((id (*)(id, SEL, NSTableView *, NSTableColumn *, NSInteger))objc_msgSend)(self, @selector(mod_tableView:objectValueForTableColumn:row:), aTableView, aTableColumn, rowIndex);
            if([obj isKindOfClass:[NSMutableAttributedString class]]) {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSFont *font = [NSFont fontWithName:[defaults stringForKey:@"BSTFontName"] size:[defaults floatForKey:@"BSTFontSize"]];
                if(!font) font = [NSFont systemFontOfSize:[defaults floatForKey:@"BSTFontSize"]];
                else if([font.fontName isEqualToString:@".SFNSText"] || [font.fontName isEqualToString:@".SFNSDisplay"]) {
                    font = [NSFont systemFontOfSize:font.pointSize];
                }
                [obj addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [obj length])];
            }
            return obj;
        };
        SWIZZLE_METHOD(ThreadList, tableView:objectValueForTableColumn:row:, objectValueForTableColumn_mod, "@@:@:@:q");
    }
    
#if MAC_OS_X_VERSION_MAX_ALLOWED < 101500
    void (^willDisplayCell_mod)(id, NSTableView *, id, NSTableColumn *, NSInteger) = ^(id self, NSTableView * aTableView, id aCell, NSTableColumn *aTableColumn, NSInteger rowIndex) {
        ((void (*)(id, SEL, NSTableView *, id, NSTableColumn *, NSInteger))objc_msgSend)(self, @selector(mod_tableView:willDisplayCell:forTableColumn:row:), aTableView, aCell, aTableColumn, rowIndex);
        if([aCell isKindOfClass:[NSTextFieldCell class]]) {
            [aCell setVerticalCentering:YES];
        }
    };
    SWIZZLE_METHOD(Browser, tableView:willDisplayCell:forTableColumn:row:, willDisplayCell_mod, "v@:@:@:@:q");
#else
    void (^drawInteriorWithFrame_mod)(NSTextFieldCell *, NSRect, NSView *) = ^(NSTextFieldCell *self, NSRect cellFrame, NSView *controlView) {
        static Class targetClass = nil;
        if (!targetClass) {
            targetClass = objc_lookUpClass("ThreadsListTable");
        }
        if (![controlView isMemberOfClass:targetClass]) {
            ((void (*)(id, SEL, NSRect, NSView *))objc_msgSend)(self, @selector(mod_drawInteriorWithFrame:inView:), cellFrame, controlView);
            return;
        }
        NSAttributedString *attrString = self.attributedStringValue;
        
        /* if your values can be attributed strings, make them white when selected */
        if (self.isHighlighted && self.backgroundStyle==NSBackgroundStyleDark) {
            NSMutableAttributedString *whiteString = attrString.mutableCopy;
            [whiteString addAttribute: NSForegroundColorAttributeName
                                value: [NSColor alternateSelectedControlTextColor]
                                range: NSMakeRange(0, whiteString.length) ];
            attrString = whiteString;
        }
        
        [attrString drawWithRect: [self mod_titleRectForBounds:cellFrame]
                         options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin];
    };
    SWIZZLE_METHOD([NSTextFieldCell class], drawInteriorWithFrame:inView:, drawInteriorWithFrame_mod, "v@:{CGRect={CGPoint=dd}{CGSize=dd}}:@");
#endif
    
    NSDictionary* (^paragraphStyleAttrForIdentifier_mod)(id, NSString *) = ^(id self, NSString *identifier) {
        NSDictionary *dic;
        if([identifier isEqualToString:@"Title"]) {
            NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            style.lineBreakMode = [defaults integerForKey:@"BSTThreadTitleLineBreakMode"];
            dic = [NSDictionary dictionaryWithObject:style forKey:NSParagraphStyleAttributeName];
        }
        else dic = [self performSelector:@selector(mod_paragraphStyleAttrForIdentifier:) withObject:identifier];
        return dic;
    };
    SWIZZLE_METHOD(ThreadList, paragraphStyleAttrForIdentifier:, paragraphStyleAttrForIdentifier_mod, "@@:@");
    
    self.status = BSTPluginStatusEnabled;
}

@end
