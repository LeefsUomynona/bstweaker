//
//  BSTweaker.m
//  BSTweaker
//
//  Created by anonymous on 2017/12/02.
//  Copyright © 2017年 anonymous. All rights reserved.
//

#import "BSTweaker.h"
#import "BSTPlugIns.h"

static NSString* const statusMessages[] = {
    @"この機能は現在無効です。",
    @"この機能は現在有効です。",
    @"この機能は現在無効です。このバージョンの BathyScaphe では不要です。",
    @"この機能は現在無効です。有効化に失敗しました。",
};

static NSAttributedString *attributedFontNameString(NSFont *font)
{
    NSString *str = [NSString stringWithFormat:@"%@ %.1f",font.displayName,font.pointSize];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.alignment = NSCenterTextAlignment;
    NSDictionary *attr = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont fontWithName:font.fontName size:13],NSFontAttributeName,style,NSParagraphStyleAttributeName,nil];
    return [[NSAttributedString alloc] initWithString:str attributes:attr];
}

static NSArray *systemSounds(void)
{
    static NSArray *systemSounds;
    if (!systemSounds) {
        NSMutableArray *returnArr = [[NSMutableArray alloc] init];
        NSEnumerator *librarySources = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSAllDomainsMask, YES) objectEnumerator];
        NSString *sourcePath;
        while(sourcePath = [librarySources nextObject]) {
            NSEnumerator *soundSource = [[NSFileManager defaultManager] enumeratorAtPath:[sourcePath stringByAppendingPathComponent:@"Sounds"]];
            NSString *soundFile;
            while(soundFile = [soundSource nextObject])
                if ([NSSound soundNamed:[soundFile stringByDeletingPathExtension]])
                    [returnArr addObject:[soundFile stringByDeletingPathExtension]];
        }
        systemSounds = [[NSArray alloc] initWithArray:[returnArr sortedArrayUsingSelector:@selector(compare:)]];
    }
    return systemSounds;
}

NSColor *BSTReplyNotifierBGColor;
NSColor *BSTReplyNotifierAltBGColor;

@interface BSTweaker ()
@property(nonatomic, weak) id appDefaults;
@property(nonatomic, strong) IBOutlet NSView *o_view;
@property(nonatomic, weak) IBOutlet NSTextField *o_threadHeightField;
@property(nonatomic, weak) IBOutlet NSStepper *o_threadHeightStepper;
@property(nonatomic, weak) IBOutlet NSPopUpButton *o_boardHeightPopup;
@property(nonatomic, weak) IBOutlet NSButton *o_use5chEnabler;
@property(nonatomic, weak) IBOutlet NSButton *o_useBoardListRowHeightChanger;
@property(nonatomic, weak) IBOutlet NSButton *o_useThreadListAppearanceChanger;
@property(nonatomic, weak) IBOutlet NSButton *o_useThrobberRemover;
@property(nonatomic, weak) IBOutlet NSTextField *o_use5chEnablerStatus;
@property(nonatomic, weak) IBOutlet NSTextField *o_useKakikomiLogStatus;
@property(nonatomic, weak) IBOutlet NSTextField *o_useBoardListRowHeightChangerStatus;
@property(nonatomic, weak) IBOutlet NSTextField *o_useThreadListAppearanceChangerStatus;
@property(nonatomic, weak) IBOutlet NSTextField *o_useAdvancedNGFilterStatus;
@property(nonatomic, weak) IBOutlet NSTextField *o_useReplyNotifierStatus;
@property(nonatomic, weak) IBOutlet NSTextField *o_useThrobberRemoverStatus;
@property(nonatomic, weak) IBOutlet NSTextField *o_useDig5chSearcherStatus;
@property(nonatomic, weak) IBOutlet NSTextField *o_useAlertSuppressorStatus;
@property(nonatomic, weak) IBOutlet NSTextField *o_useYenToBackslashConverterStatus;
@property(nonatomic, weak) IBOutlet NSTextField *o_useLinkCompletionStatus;
@property(nonatomic, weak) IBOutlet NSTextField *o_useBrowserSelectorStatus;
@property(nonatomic, weak) IBOutlet NSTextField *o_useJapaneseEraConverterStatus;
@property(nonatomic, weak) IBOutlet NSButton *o_threadFontButton;
@property(nonatomic, weak) IBOutlet NSColorWell *o_replyBGColor;
@property(nonatomic, weak) IBOutlet NSButton *o_shouldChangeReplyBGColor;
@property(nonatomic, weak) IBOutlet NSButton *o_shouldSendReplyNotification;
@property(nonatomic, weak) IBOutlet NSButton *o_shouldDisplayBoardListIcon;
@property(nonatomic, weak) IBOutlet NSButton *o_shouldBookmarkMyReply;
@property(nonatomic, weak) IBOutlet NSButton *o_shouldChangeReplyAltBGColor;
@property(nonatomic, weak) IBOutlet NSColorWell *o_replyAltBGColor;
@property(nonatomic, weak) IBOutlet NSPopUpButton *o_threadTitleLineBreakModePopup;
@property(nonatomic, weak) IBOutlet NSTextField *o_APIServerTimeoutField;
@property(nonatomic, weak) IBOutlet NSButton *o_shouldRoute2chTo5ch;
@property(nonatomic, weak) IBOutlet NSPopUpButton *o_notificationSound;
@property(nonatomic, weak) IBOutlet NSPopUpButton *o_preferredBrowser;
@property(nonatomic, weak) IBOutlet NSButton *o_force5chHTTPS;
@property(nonatomic, strong) NSMutableArray *preferredBrowsers;
@property(nonatomic, copy) NSString *threadNGFilterStatusText;
@end

@implementation BSTweaker

+ (void)initialize
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults registerDefaults:@{
            @"BSRHCThreadListHeight" : @(17),
            @"BSRHCBoardListHeight" : @(2),
            @"BSTFontName" : [NSFont systemFontOfSize:12].fontName,
            @"BSTFontSize" : @(12.0),
            @"BSTThreadTitleLineBreakMode" : @(4),
            @"BSTReplyNotifierBGColor" : [NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.95 green:0.85 blue:0.85 alpha:1.0]],
            @"BSTReplyNotifierShouldChangeBGColor" : @(NSOffState),
            @"BSTReplyNotifierAltBGColor" : [NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.8 green:0.95 blue:1.0 alpha:1.0]],
            @"BSTReplyNotifierShouldChangeAltBGColor" : @(NSOffState),
            @"BSTReplyNotifierShouldSendNotification" : @(NSOnState),
            @"BSTReplyNotifierShouldBookmarkMyReply" : @(NSOffState),
            @"BSTAPIServerTimeout" : @(10),
            @"BSTRoute2chTo5ch" : @(NSOffState),
            @"BSTNotificationSound" : @"なし",
            @"BSTPreferredBrowser" : [[[NSWorkspace sharedWorkspace] URLForApplicationToOpenURL:[NSURL URLWithString:@"http://"]] path],
            @"BSTForce5chHTTPS" : @(NSOffState),
            @"BSTUse5chEnabler" : @(NSOnState),
            @"BSTUseKakikomiLog" : @(NSOnState),
            @"BSTUseBoardListHeightChanger" : @(NSOnState),
            @"BSTUseThreadListAppearanceChanger" : @(NSOnState),
            @"BSTUseAdvancedNGFilter" : @(NSOnState),
            @"BSTUseReplyNotifier" : @(NSOnState),
            @"BSTUseThrobberRemover" : @(NSOnState),
            @"BSTUseDig5chSearcher" : @(NSOffState),
            @"BSTUseAlertSuppressor" : @(NSOffState),
            @"BSTUseYenToBackslashConverter" : @(NSOnState),
            @"BSTUseLinkCompletion" : @(NSOffState),
            @"BSTUseBrowserSelector" : @(NSOnState),
            @"BSTUseJapaneseEraConverter" : @(NSOffState),
            @"BSTUseThreadNGFilter" : @(NSOffState),
        }];
        
        if([[defaults objectForKey:@"BSTUse5chEnabler"] integerValue] == NSOnState) {
            [[BST5chEnabler sharedInstance] swizzle];
        }
        if([[defaults objectForKey:@"BSTUseKakikomiLog"] integerValue] == NSOnState) {
            [[BSTKakikomiLog sharedInstance] swizzle];
        }
        if([[defaults objectForKey:@"BSTUseBoardListHeightChanger"] integerValue] == NSOnState) {
            [[BSTBoardListHeightChanger sharedInstance] swizzle];
        }
        if([[defaults objectForKey:@"BSTUseThreadListAppearanceChanger"] integerValue] == NSOnState) {
            [[BSTThreadListAppearanceChanger sharedInstance] swizzle];
        }
        if([[defaults objectForKey:@"BSTUseAdvancedNGFilter"] integerValue] == NSOnState) {
            [[BSTAdvancedNGFilter sharedInstance] swizzle];
        }
        if([[defaults objectForKey:@"BSTUseReplyNotifier"] integerValue] == NSOnState) {
            [[BSTReplyNotifier sharedInstance] swizzle];
            if([defaults integerForKey:@"BSTReplyNotifierShouldChangeBGColor"] == NSOnState) {
                NSData *colorData = [[NSUserDefaults standardUserDefaults] dataForKey:@"BSTReplyNotifierBGColor"];
                if(colorData) BSTReplyNotifierBGColor = (NSColor *)[NSUnarchiver unarchiveObjectWithData:colorData];
            }
            if([defaults integerForKey:@"BSTReplyNotifierShouldChangeAltBGColor"] == NSOnState) {
                NSData *colorData = [[NSUserDefaults standardUserDefaults] dataForKey:@"BSTReplyNotifierAltBGColor"];
                if(colorData) BSTReplyNotifierAltBGColor = (NSColor *)[NSUnarchiver unarchiveObjectWithData:colorData];
            }
        }
        if([[defaults objectForKey:@"BSTUseThrobberRemover"] integerValue] == NSOnState) {
            [[BSTThrobberRemover sharedInstance] swizzle];
        }
        if([[defaults objectForKey:@"BSTUseDig5chSearcher"] integerValue] == NSOnState) {
            [[BSTDig5chSearcher sharedInstance] swizzle];
        }
        if([[defaults objectForKey:@"BSTUseAlertSuppressor"] integerValue] == NSOnState) {
            [[BSTAlertSuppressor sharedInstance] swizzle];
        }
        if([[defaults objectForKey:@"BSTUseYenToBackslashConverter"] integerValue] == NSOnState) {
            [[BSTYenToBackslashConverter sharedInstance] swizzle];
        }
        if([[defaults objectForKey:@"BSTUseLinkCompletion"] integerValue] == NSOnState) {
            [[BSTLinkCompletion sharedInstance] swizzle];
        }
        if([[defaults objectForKey:@"BSTUseBrowserSelector"] integerValue] == NSOnState) {
            [[BSTBrowserSelector sharedInstance] swizzle];
        }
        if([[defaults objectForKey:@"BSTUseJapaneseEraConverter"] integerValue] == NSOnState) {
            [[BSTJapaneseEraConverter sharedInstance] swizzle];
        }
        if([[defaults objectForKey:@"BSTUseThreadNGFilter"] integerValue] == NSOnState) {
            [[BSTThreadListNGFilter sharedInstance] swizzle];
        }
        [[BSTHotfix sharedInstance] swizzle];
    });
}

- (BOOL)registerAppForPath:(NSString *)path
{
    NSBundle *appBundle = [NSBundle bundleWithPath:path];
    if(!appBundle) return NO;
    NSString *appName = [[appBundle infoDictionary] objectForKey:@"CFBundleDisplayName"];
    if(!appName) appName = [[appBundle infoDictionary] objectForKey:@"CFBundleName"];
    if(!appName) return NO;
    NSMenuItem *dupItem = [_o_preferredBrowser itemWithTitle:appName];
    if(dupItem) {
        appName = [NSString stringWithFormat:@"%@ (%@)", appName, [[appBundle infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
        dupItem = [_o_preferredBrowser itemWithTitle:appName];
        int i;
        NSString *baseAppName = appName;
        for(i=1;dupItem != nil;i++) {
            appName = [NSString stringWithFormat:@"%@ (%d)", baseAppName, i];
            dupItem = [_o_preferredBrowser itemWithTitle:appName];
        }
    }
    [_o_preferredBrowser insertItemWithTitle:appName atIndex:_preferredBrowsers.count];
    NSMenuItem *item = [_o_preferredBrowser itemAtIndex:_preferredBrowsers.count];
    NSImage *icon = [[NSWorkspace sharedWorkspace] iconForFile:path];
    [icon setSize:NSMakeSize(16, 16)];
    [item setImage:icon];
    [_preferredBrowsers addObject:path];
    return YES;
}

- (id)initWithPreferences:(AppDefaults *)prefs
{
    self = [self init];
    if(self) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [NSBundle loadNibNamed:@"Prefs" owner:self];
        [_o_threadHeightField setIntValue:[_o_threadHeightStepper intValue]];
        [_o_notificationSound addItemsWithTitles:systemSounds()];
        [_o_notificationSound selectItemWithTitle:[defaults stringForKey:@"BSTNotificationSound"]];
        _preferredBrowsers = [[NSMutableArray alloc] init];
        NSArray *handlers = (__bridge_transfer NSArray *) LSCopyAllHandlersForURLScheme((__bridge CFStringRef) @"https");
        for(NSString *bundle in handlers) {
            NSString *path = [[NSWorkspace sharedWorkspace] absolutePathForAppBundleWithIdentifier:bundle];
            if(path && [path isEqualToString:[@"/Applications" stringByAppendingPathComponent:[path lastPathComponent]]]) {
                [self registerAppForPath:path];
            }
        }
        NSString *defaultBrowserPath = [[[NSWorkspace sharedWorkspace] URLForApplicationToOpenURL:[NSURL URLWithString:@"http://"]] path];
        if(![_preferredBrowsers containsObject:defaultBrowserPath]) {
            [self registerAppForPath:defaultBrowserPath];
        }
        NSString *selectedBrowserPath = [defaults objectForKey:@"BSTPreferredBrowser"];
        if(![_preferredBrowsers containsObject:selectedBrowserPath]) {
            if(![self registerAppForPath:selectedBrowserPath]) selectedBrowserPath = defaultBrowserPath;
        }
        [_o_preferredBrowser selectItemAtIndex:[_preferredBrowsers indexOfObject:selectedBrowserPath]];
        [[_o_preferredBrowser menu] addItem:[NSMenuItem separatorItem]];
        [_o_preferredBrowser addItemWithTitle:@"その他..."];
        
        _appDefaults = prefs;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDefaultsLayoutSettingsUpdateNotification" object:_appDefaults];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:@"BSTThreadListNGFilterRuleUpdated" object:nil];
    }
    return self;
}

- (BOOL)previewLink:(NSURL *)url
{
    return NO;
}

- (BOOL)validateLink:(NSURL *)url
{
    return NO;
}

- (NSString *)identifierString
{
    NSBundle *bundle = [NSBundle bundleWithIdentifier:@"jp.anonymous.BSTweaker"];
    return [NSString stringWithFormat:@"BSTweaker (%@/%@)",[[bundle infoDictionary] objectForKey:@"CFBundleShortVersionString"],[[bundle infoDictionary] objectForKey:@"CFBundleVersion"]];
}

- (NSView *)preferenceView
{
    float bundleVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] floatValue];
    _o_use5chEnablerStatus.stringValue = statusMessages[[BST5chEnabler sharedInstance].status];
    _o_useKakikomiLogStatus.stringValue = statusMessages[[BSTKakikomiLog sharedInstance].status];
    _o_useBoardListRowHeightChangerStatus.stringValue = statusMessages[[BSTBoardListHeightChanger sharedInstance].status];
    _o_useThreadListAppearanceChangerStatus.stringValue = statusMessages[[BSTThreadListAppearanceChanger sharedInstance].status];
    _o_useAdvancedNGFilterStatus.stringValue = statusMessages[[BSTAdvancedNGFilter sharedInstance].status];
    _o_useReplyNotifierStatus.stringValue = statusMessages[[BSTReplyNotifier sharedInstance].status];
    _o_useThrobberRemoverStatus.stringValue = statusMessages[[BSTThrobberRemover sharedInstance].status];
    _o_useDig5chSearcherStatus.stringValue = statusMessages[[BSTDig5chSearcher sharedInstance].status];
    _o_useAlertSuppressorStatus.stringValue = statusMessages[[BSTAlertSuppressor sharedInstance].status];
    _o_useYenToBackslashConverterStatus.stringValue = statusMessages[[BSTYenToBackslashConverter sharedInstance].status];
    _o_useLinkCompletionStatus.stringValue = statusMessages[[BSTLinkCompletion sharedInstance].status];
    _o_useBrowserSelectorStatus.stringValue = statusMessages[[BSTBrowserSelector sharedInstance].status];
    _o_useJapaneseEraConverterStatus.stringValue = statusMessages[[BSTJapaneseEraConverter sharedInstance].status];
    if(!_threadNGFilterStatusText) self.threadNGFilterStatusText = statusMessages[[BSTThreadListNGFilter sharedInstance].status];
    
    if([BST5chEnabler sharedInstance].status != BSTPluginStatusEnabled) {
        _o_APIServerTimeoutField.enabled = NO;
        _o_shouldRoute2chTo5ch.enabled = NO;
        _o_force5chHTTPS.enabled = NO;
        if([BST5chEnabler sharedInstance].status == BSTPluginStatusBlockedByVersion) {
            _o_use5chEnabler.enabled = NO;
        }
    }
    /*else if(bundleVersion > 1057) {
        _o_shouldRoute2chTo5ch.enabled = NO;
    }*/
    if([BSTBoardListHeightChanger sharedInstance].status != BSTPluginStatusEnabled) {
        _o_boardHeightPopup.enabled = NO;
        _o_shouldDisplayBoardListIcon.enabled = NO;
        if([BSTBoardListHeightChanger sharedInstance].status == BSTPluginStatusBlockedByVersion) {
            _o_useBoardListRowHeightChanger.enabled = NO;
        }
    }
    else {
        if([_appDefaults respondsToSelector:@selector(setBoardListShowsIcon:)] && [_appDefaults respondsToSelector:@selector(boardListShowsIcon)]) {
            _o_shouldDisplayBoardListIcon.state = [_appDefaults performSelector:@selector(boardListShowsIcon)] ? NSOnState : NSOffState;
        }
        else _o_shouldDisplayBoardListIcon.enabled = NO;
    }
    if([BSTThreadListAppearanceChanger sharedInstance].status != BSTPluginStatusEnabled) {
        _o_threadHeightStepper.enabled = NO;
        _o_threadHeightField.enabled = NO;
        _o_threadFontButton.enabled = NO;
        _o_threadTitleLineBreakModePopup.enabled = NO;
        if([BSTThreadListAppearanceChanger sharedInstance].status == BSTPluginStatusBlockedByVersion) {
            _o_useThreadListAppearanceChanger.enabled = NO;
        }
    }
    else {
        float shortVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] floatValue];
        if(bundleVersion < 1057 || shortVersion < 3.0f) {
            _o_threadHeightStepper.enabled = NO;
            _o_threadHeightField.enabled = NO;
            _o_threadFontButton.enabled = NO;
        }
    }
    if([BSTReplyNotifier sharedInstance].status != BSTPluginStatusEnabled) {
        _o_shouldChangeReplyBGColor.enabled = NO;
        _o_shouldSendReplyNotification.enabled = NO;
        _o_replyBGColor.enabled = NO;
        _o_shouldBookmarkMyReply.enabled = NO;
        _o_notificationSound.enabled = NO;
    }
    else {
        if(_o_shouldChangeReplyBGColor.state == NSOnState) _o_replyBGColor.enabled = YES;
        else _o_replyBGColor.enabled = NO;
        if(_o_shouldChangeReplyAltBGColor.state == NSOnState) _o_replyAltBGColor.enabled = YES;
        else _o_replyAltBGColor.enabled = NO;
        if(_o_shouldSendReplyNotification.state == NSOnState) _o_notificationSound.enabled = YES;
        else _o_notificationSound.enabled = NO;
    }
    if([BSTThrobberRemover sharedInstance].status != BSTPluginStatusEnabled) {
        if([BSTThrobberRemover sharedInstance].status == BSTPluginStatusBlockedByVersion) {
            _o_useThrobberRemover.enabled = NO;
        }
    }
    if([BSTBrowserSelector sharedInstance].status != BSTPluginStatusEnabled) {
        _o_preferredBrowser.enabled = NO;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSFont *font = [NSFont fontWithName:[defaults stringForKey:@"BSTFontName"] size:[defaults floatForKey:@"BSTFontSize"]];
    if(!font) font = [NSFont systemFontOfSize:[defaults floatForKey:@"BSTFontSize"]];
    _o_threadFontButton.attributedTitle = attributedFontNameString(font);
    return _o_view;
}

- (IBAction)valueChanged:(id)sender
{
    if(sender == _o_threadHeightStepper) {
        [_o_threadHeightField setStringValue:[sender stringValue]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDefaultsLayoutSettingsUpdateNotification" object:_appDefaults];
    }
    else if(sender == _o_boardHeightPopup || sender == _o_threadTitleLineBreakModePopup) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDefaultsLayoutSettingsUpdateNotification" object:_appDefaults];
    }
    else if(sender == _o_replyBGColor || sender == _o_replyAltBGColor) {
        if(sender == _o_replyBGColor) BSTReplyNotifierBGColor = [_o_replyBGColor color];
        else BSTReplyNotifierAltBGColor = [_o_replyAltBGColor color];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDefaultsThreadViewThemeDidChangeNotification" object:_appDefaults];
    }
    else if(sender == _o_shouldChangeReplyBGColor) {
        if([sender state] == NSOnState) {
            _o_replyBGColor.enabled = YES;
            BSTReplyNotifierBGColor = [_o_replyBGColor color];
        }
        else {
            _o_replyBGColor.enabled = NO;
            BSTReplyNotifierBGColor = nil;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDefaultsThreadViewThemeDidChangeNotification" object:_appDefaults];
    }
    else if(sender == _o_shouldChangeReplyAltBGColor) {
        if([sender state] == NSOnState) {
            _o_replyAltBGColor.enabled = YES;
            BSTReplyNotifierAltBGColor = [_o_replyAltBGColor color];
        }
        else {
            _o_replyAltBGColor.enabled = NO;
            BSTReplyNotifierAltBGColor = nil;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDefaultsThreadViewThemeDidChangeNotification" object:_appDefaults];
    }
    else if(sender == _o_shouldDisplayBoardListIcon) {
         ((void (*)(id, SEL, BOOL))objc_msgSend)(_appDefaults, @selector(setBoardListShowsIcon:), [sender state] == NSOnState);
    }
    else if(sender == _o_shouldSendReplyNotification) {
        if([sender state] == NSOnState) _o_notificationSound.enabled = YES;
        else _o_notificationSound.enabled = NO;
    }
    else if(sender == _o_notificationSound) {
        if([sender indexOfSelectedItem] != 0) {
            [[NSSound soundNamed:[[sender selectedItem] title]] play];
        }
    }
    else if(sender == _o_preferredBrowser) {
        NSUInteger selected = [_o_preferredBrowser indexOfSelectedItem];
        if(selected < _preferredBrowsers.count) {
            [[NSUserDefaults standardUserDefaults] setObject:_preferredBrowsers[selected] forKey:@"BSTPreferredBrowser"];
        } else {
            NSArray *appsDirs = NSSearchPathForDirectoriesInDomains(NSApplicationDirectory, NSLocalDomainMask, YES);
            NSString *appsDir = nil;
            if([appsDirs count]) appsDir = [appsDirs objectAtIndex:0];
            NSOpenPanel *openPanel = [NSOpenPanel openPanel];
            [openPanel setAllowsMultipleSelection:NO];
            [openPanel setCanChooseDirectories:NO];
            [openPanel setAllowedFileTypes:[NSArray arrayWithObject:NSFileTypeForHFSTypeCode('APPL')]];
            [openPanel setDirectoryURL:[NSURL fileURLWithPath:appsDir]];
            if([openPanel runModal] == NSOKButton) {
                NSString *selectedPath = openPanel.URL.path;
                BOOL success = YES;
                if(![_preferredBrowsers containsObject:selectedPath]) {
                    success = [self registerAppForPath:selectedPath];
                }
                if(success) [[NSUserDefaults standardUserDefaults] setObject:selectedPath forKey:@"BSTPreferredBrowser"];
            }
            [_o_preferredBrowser selectItemAtIndex:[_preferredBrowsers indexOfObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"BSTPreferredBrowser"]]];
        }
    }
}

- (IBAction)selectFont:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSUInteger modifierFlags = [NSEvent modifierFlags];
    if(modifierFlags & NSCommandKeyMask) {
        NSFont *font = [NSFont systemFontOfSize:[defaults floatForKey:@"BSTFontSize"]];
        [defaults setValue:font.fontName forKey:@"BSTFontName"];
        _o_threadFontButton.attributedTitle = attributedFontNameString(font);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDefaultsLayoutSettingsUpdateNotification" object:_appDefaults];
    }
    else {
        NSFontManager *fontManager = [NSFontManager sharedFontManager];
        [fontManager setDelegate:self];
        [fontManager setTarget:self];
        NSFontPanel *fp = [NSFontPanel sharedFontPanel];
        NSFont *font = [NSFont fontWithName:[defaults stringForKey:@"BSTFontName"] size:[defaults floatForKey:@"BSTFontSize"]];
        [fp setPanelFont:font isMultiple:NO];
        [fp makeKeyAndOrderFront:nil];
    }
}

- (NSUInteger)validModesForFontPanel:(NSFontPanel *)fontPanel
{
    return NSFontPanelFaceModeMask|NSFontPanelCollectionModeMask;
}

- (void)changeFont:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSFont *oldFont = [NSFont fontWithName:[defaults stringForKey:@"BSTFontName"] size:[defaults floatForKey:@"BSTFontSize"]];
    NSFont *newFont = [[NSFontManager sharedFontManager] convertFont:oldFont];
    //NSLog(@"%@",newFont);
    if([newFont.fontName isEqualToString:@".SFNSText"] || [newFont.fontName isEqualToString:@".SFNSDisplay"]) {
        newFont = [NSFont systemFontOfSize:newFont.pointSize];
    }
    [[NSFontManager sharedFontManager] setSelectedFont:newFont isMultiple:NO];
    [defaults setValue:newFont.fontName forKey:@"BSTFontName"];
    [defaults setFloat:newFont.pointSize forKey:@"BSTFontSize"];
    _o_threadFontButton.attributedTitle = attributedFontNameString(newFont);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDefaultsLayoutSettingsUpdateNotification" object:_appDefaults];
}

- (void)didReceiveNotification:(NSNotification *)aNotification
{
    if([aNotification.name isEqualToString:@"BSTThreadListNGFilterRuleUpdated"]) {
        NSUInteger rulesCount = [aNotification.userInfo[@"RulesCount"] unsignedIntegerValue];
        self.threadNGFilterStatusText = [NSString stringWithFormat:@"%@ %lu 個のルールが登録されています。",statusMessages[[BSTThreadListNGFilter sharedInstance].status],rulesCount];
    }
}

@end
