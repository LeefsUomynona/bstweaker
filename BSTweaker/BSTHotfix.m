//
//  BSTHotfix.m
//  BSTweaker
//
//  Created by anonymous on 2018/02/17.
//  Copyright © 2018年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <sys/sysctl.h>
#import <libxml/HTMLparser.h>
#import <libxml/xpath.h>
#import "BSTPlugIns.h"

@interface BSTHotfix ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@property(nonatomic, readonly) BOOL shouldEnableZWNJRemover;
@property(nonatomic, readonly) BOOL shouldEnableNavigatorFix;
@property(nonatomic, readonly) BOOL shouldEnableHTMLParserFix;
@end

static NSString* const preferredSystemVersionPath = @"/System/Library/CoreServices/.SystemVersionPlatform.plist";
static NSString* const defaultSystemVersionPath = @"/System/Library/CoreServices/SystemVersion.plist";

static void fixSegmentedControlPosition(NSView *control)
{
    if (control == nil) return;
    NSRect frame = control.frame;
    NSRect superFrame = control.superview.frame;
    if (superFrame.size.width - frame.origin.x != 121) return;
    if (frame.size.width != 113) return;
    if (frame.size.height != 16) return;
    frame.origin.x -= 20;
    frame.size.width += 15;
    control.frame = frame;
}

@implementation BSTHotfix

+ (id)sharedInstance
{
    static BSTHotfix* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTHotfix alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if(self) {
        float bundleVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] floatValue];
        float shortVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] floatValue];
        NSString *systemVersionPlistPath = defaultSystemVersionPath;
        if([[NSFileManager defaultManager] fileExistsAtPath:preferredSystemVersionPath]) {
            systemVersionPlistPath = preferredSystemVersionPath;
        }
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:systemVersionPlistPath];
        NSString *version = [dict objectForKey:@"ProductVersion"];
        
        if([version hasPrefix:@"10.13"]) {
            float minor =  [version substringFromIndex:3].floatValue;
            if(minor == 13.3f) {
                size_t size = 64;
                char build[64];
                sysctlbyname("kern.osversion", build, &size, NULL, 0);
                if(!strcmp(build,"17D47") || !strcmp(build,"17D2047")) {
                    _shouldEnableZWNJRemover = YES;
                }
            }
        }
        if (bundleVersion <= 1089 && shortVersion == 3.1f) {
            /* SystemVersion.plist reports 11.0 as 10.16 when SYSTEM_VERSION_COMPAT=1 */
            if (![version hasPrefix:@"10."]) {
                _shouldEnableNavigatorFix = YES;
            }
            else {
                float minor =  [version substringFromIndex:3].floatValue;
                if (minor >= 16.f) _shouldEnableNavigatorFix = YES;
            }
        }
        if(shortVersion > 2.6f && version.floatValue >= 12.0) {
            _shouldEnableHTMLParserFix = YES;
        }
    }
    return self;
}

- (void)swizzle
{
    if(_status != BSTPluginStatusDisabled) return;
    
    if (_shouldEnableZWNJRemover) {
        Class ThreadListItem = objc_lookUpClass("BSThreadListItem");
        Class DatabaseManager = objc_lookUpClass("DatabaseManager");
        if(!ThreadListItem || !DatabaseManager) {
            self.status = BSTPluginStatusFailedToLoad;
            return;
        }
        
        NSString* (^threadName_mod)(id, NSMutableString *) = ^(id self, NSMutableString *string) {
            NSString *str = [self performSelector:@selector(mod_threadName)];
            str = [str stringByReplacingOccurrencesOfString:@"\u200c" withString:@"" options:NSLiteralSearch range:NSMakeRange(0, [str length])];
            return str;
        };
        SWIZZLE_METHOD(ThreadListItem, threadName, threadName_mod, "@@");
        
        
        NSString* (^threadTitleFromBoardName_mod)(id, NSString*, NSString*) = ^(id self, NSString *boardName, NSString *identifier) {
            NSString *str = ((NSString* (*)(id, SEL, NSString*, NSString*))objc_msgSend)(self, @selector(mod_threadTitleFromBoardName:threadIdentifier:), boardName, identifier);
            str = [str stringByReplacingOccurrencesOfString:@"\u200c" withString:@"" options:NSLiteralSearch range:NSMakeRange(0, [str length])];
            return str;
        };
        SWIZZLE_METHOD(DatabaseManager, threadTitleFromBoardName:threadIdentifier:, threadTitleFromBoardName_mod, "@@:@:@");
        
        void (^replaceEntityReference_mod)(id) = ^(id self) {
            [self replaceOccurrencesOfString:@"&#8204;" withString:@"" options:NSLiteralSearch range:NSMakeRange(0,[self length])];
            [self performSelector:@selector(mod_replaceEntityReference)];
        };
        SWIZZLE_METHOD([NSMutableString class], replaceEntityReference, replaceEntityReference_mod, "v@");
    }
    if (_shouldEnableNavigatorFix) {
        Class WindowController = objc_lookUpClass("CMRStatusLineWindowController");
        if(!WindowController) {
            self.status = BSTPluginStatusFailedToLoad;
            return;
        }
        
        NSArray *curWindows = [NSApp orderedWindows];
        for(NSWindow *win in curWindows) {
            if([win.windowController isKindOfClass:WindowController]) {
                NSView *segmentedControl = [win.windowController performSelector:@selector(indexingNavigator)];
                fixSegmentedControlPosition(segmentedControl);
            }
        }
        
        void (^windowDidLoad_mod)(id) = ^(id self) {
            [self performSelector:@selector(mod_windowDidLoad)];
            NSView *segmentedControl = [self performSelector:@selector(indexingNavigator)];
            fixSegmentedControlPosition(segmentedControl);
        };
        SWIZZLE_METHOD(WindowController, windowDidLoad, windowDidLoad_mod, "v@");
    }
    if(_shouldEnableHTMLParserFix) {
        Class SG2chErrorHandler = objc_lookUpClass("SG2chErrorHandler");
        xmlInitParser();
        BOOL (^parseHTMLContents_mod)(id, NSData *, NSString **, NSString **, unsigned int) = ^(id self, NSData *htmlContents, NSString **intoTitle, NSString **intoMessage, unsigned int suggestedEncoding) {
            BOOL ret = ((BOOL (*)(id, SEL, NSData*, NSString**, NSString **, unsigned int))objc_msgSend)(self, @selector(mod_parseHTMLContents:intoTitle:intoMessage:suggestedEncoding:), htmlContents, intoTitle, intoMessage, suggestedEncoding);
            if(ret && ((intoTitle && !*intoTitle) || (intoMessage && !*intoMessage))) {
                htmlParserCtxtPtr htmlParesr = htmlNewParserCtxt();
                htmlDocPtr doc = htmlCtxtReadMemory(htmlParesr, htmlContents.bytes, (int)htmlContents.length, NULL, NULL, HTML_PARSE_RECOVER|HTML_PARSE_NOERROR|HTML_PARSE_NOWARNING);
                if(doc) {
                    xmlXPathContextPtr xpctx = xmlXPathNewContext(doc);
                    xmlXPathObjectPtr xpobj = xmlXPathEvalExpression((xmlChar *)"/html/head/title|/html/body", xpctx);
                    xmlNodeSetPtr nodes = xpobj->nodesetval;
                    for(int i=0; i<nodes->nodeNr; i++) {
                        xmlNodePtr node = nodes->nodeTab[i];
                        if(intoTitle && !*intoTitle && !strcasecmp((const char *)node->name, "title")) {
                            const char *contents = (const char *)xmlNodeGetContent(node);
                            if(contents) *intoTitle = [NSString stringWithUTF8String:contents];
                        }
                        if(intoMessage && !*intoMessage && !strcasecmp((const char *)node->name, "body")) {
                            const char *contents = (const char *)xmlNodeGetContent(node);
                            if(contents) *intoMessage = [NSString stringWithUTF8String:contents];
                        }
                    }
                    xmlXPathFreeObject(xpobj);
                    xmlXPathFreeContext(xpctx);
                    xmlFreeDoc(doc);
                }
                htmlFreeParserCtxt(htmlParesr);
            }
            return ret;
        };
        SWIZZLE_METHOD(SG2chErrorHandler, parseHTMLContents:intoTitle:intoMessage:suggestedEncoding:, parseHTMLContents_mod, "c@:@:^@:^@:I");
    }
    
    self.status = BSTPluginStatusEnabled;
}

@end
